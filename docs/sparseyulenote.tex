\documentclass[11pt]{uwstat518}
%\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.%
%\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{setspace}
\usepackage{booktabs}% http://ctan.org/pkg/booktabs
\usepackage{array}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\newcommand{\Real}{\mbox{Re}}
\newcommand{\Imag}{\mbox{Im}}
\newcommand{\trace}{\mbox{tr }}
\newcommand{\ci}{\perp\!\!\!\perp}
\newcommand{\A}{{\bf A}}
\newcommand{\bmu}{{\bf \mu}}
\newcommand{\Z}{{\bf Z}}
\newcommand{\U}{{\bf U}}
\newcommand{\V}{{\bf V}}
\newcolumntype{M}{>{\centering\arraybackslash}m{\dimexpr}}


\newtheorem{ntheorem}{Theorem}
\linespread{1}

\title{Sparse Yule-Walker Estimation for Granger Causality of Irregular Time Series}
\author{Alex Tank}
\date{} 
\begin{document}
\maketitle
\section{Introduction}
We develop a simple method for assessing Granger causality for multiple time series under irregular sampling. The method leverages a sparse formulation of the Yule-Walker equations. Importantly, the Yule-Walker equations only depend on lagged covariance matrices of the data. There exist many methods for estimating these matrices from irregular sampled data, giving our approach great adaptability - some covariance estimation methods may be better than others for particular situations and irregular sampling schemes. Another approach for estimating Granger causality using a kernel VAR model can be found in \cite{Bahadori:2012}.
\section{Sparse Yule-Walker}
Consider a VAR(p) process:
\begin{equation}
x(t) = \sum_{k = 1}^{p} A_k x(t - k) + \epsilon(t),
\end{equation}
$x(t) \in \mathbb{R}^d$ and $\epsilon(t)$ is white noise, which can be rewritten as a VAR(1) process:
\begin{equation}
\tilde{x}(t) = \tilde{A} \tilde{x}(t - 1),
\end{equation}
where $\tilde{x(t)} = (x(t), x(t - 1), \ldots, x(t - p + 1))$ and 
\begin{align}
\tilde{A} = \left( \begin{array}{cccc}
A_1 & A_2 & \ldots & A_p \\
I_{d} & 0 & \ldots & 0 \\
0 & I_{d} & \ldots & 0 \\
\vdots & \ddots & \ddots & \vdots
\end{array} \right).
\end{align}
Right multiplying each side by $x(t-1)^T$ and taking expectations gives:
\begin{align}
E(\tilde{x}(t) \tilde{x}(t - 1)^T) = \tilde{A} E(\tilde{x}(t - 1) \tilde{x}(t - 1)^T).
\end{align}
Solving for $\tilde{A}$ gives:
\begin{align}
\hat{A} = \Sigma_{-1} \Sigma_0^{-1},
\end{align}
or, taking the transpose:
\begin{align}
\hat{A}^T = \Sigma_0^{-1} \Sigma_{1},
\end{align}
where $\tilde{\Sigma}_{1} = E(\tilde{x}(t) \tilde{x}(t + 1)^T)$ and $\tilde{\Sigma}_0 = E(\tilde{x}(t) \tilde{x}(t)^T)$. Note that to fit this model we only need estimations of $\Sigma_{1}$ and $\Sigma_0$. It may be easier to estimate these for irregularly sampled time series rather than fitting a VAR model directly to the data.
\subsection{Sparse Estimation}
We can obtain a sparse solution \cite{Han:2013} by solving the problem:
\begin{align}
\min_{M} \sum_{i,j} M_{ij} \\
\text{s.t.} ||\Sigma_0 M - \Sigma_{1}||_{\infty} < \lambda_1,
\end{align}
then set $\hat{A} = M^T$, where $\lambda_1$ controls the level of sparsity - if large the solution will be more sparse. This can be split into separate problems by solving:
\begin{align} \label{prob1}
\min_{M_{*j}} \sum_{i} M_{ij} \\
\text{s.t.} ||\Sigma_0 M_{*j} - \Sigma_{1}||_{\infty} < \lambda_1 , 
\end{align}
where we only need to solve for $j \in \{1,\ldots, p\}$ since the remainder of $\tilde{A}$ is given by blocks of the identity and blocks of zeros. The problem given in Eq. (\ref{prob1}) reduces to a linear program and can be solved via the simplex algorithm.

\section{Lagged Covariance Estimation}
There exist two main classes of methods to estimate $\Sigma_0$ and $\Sigma_1$: kernel methods and spectral methods \cite{Rehfeld:2011}. Spectral methods fit sin and cosine functions over a fixed frequency range to the time series using least squares, and then create a periodogram. The iFFT can then be used to infer the lagged covariance matrices. Kernel methods instead directly estimate the lagged covariance matrices, but weight a particular pair of time series observations by how similar their time difference is to the desired lag. 
\subsection{Obtaining a common sampling rate}
The first hurdle for both kernel and spectral methods is to determine the sampling rate, $\Delta_t$, to use for analysis. For VAR models, $\Delta_t$ determines the time difference between each lag in the model, or the time difference between lagged covariance matrices. A common approach for single time series is to use the average time interval:
\begin{align}
\Delta^i_t &= \frac{1}{N_i-1}\sum_{k = 2}^{N} t_k - t_{k-1} \\
&= \frac{t^i_{N_i} - t^i_1}{N_i - 1}.
\end{align}
For multiple time series we set
\begin{align}
\Delta_t = \max(\Delta^1_t, \ldots, \Delta^p_t).
\end{align}
We then re-standardize the observation times so that for the remainder we can assume our sampling rate is $1$: $t^i_k = \frac{t^i_k}{\Delta_t}$.
\subsection{Kernel methods}
Consider two time series, $x_i(t)$ and $x_j(t)$ which have been observed at irregular time points, $t^i_1, \ldots t^i_{N_i}$ and $t^j_1, \ldots t^j_{N_j}$. An estimate of the $k$th lagged covariance may be obtained by:
\begin{align} \label{kern}
\gamma_{ij}(k) = \frac{\sum_m^{N_i} \sum_n^{N_j} x_i(t^i_m) x_j(t^j_n) g(t^{i}_m - t^{j}_n - k)}{\sum_m^{N_i} \sum_n^{N_j} g(t^{i}_m - t^{j}_n - k)},
\end{align}
where $g$ is some kernel function and $t^{i}_m - t^{j}_n - k$ specifies how close the time difference is to $k$. A good kernel $g(x)$ should be near zero for $|x| > 1$. A common choice is a Gaussian kernel:
\begin{equation}
g(x) = \frac{1}{\sqrt{2 \pi} h} e^{- \frac{x^2}{2 h^2}},
\end{equation}
where typically $h = \frac{1}{4}$. Note that if we wish to fit a lag $p$ VAR model we must compute Eq. \ref{kern} for every pair $p$ times, leading to a $O (N^2 d^2 p)$ complexity where $N = \max(N_1, \ldots, N_d)$.

After the above is completed we can form the lagged covariance estimates as
\begin{align}
(\Sigma_k)_{ij} = \gamma_{ij}(k).
\end{align}
We can then construct $\tilde{\Sigma}_0$ and $\tilde{\Sigma}_1$ by:
\begin{align}
\tilde{\Sigma_0} = \left(\begin{array}{cccc}
  \Sigma_0 & \Sigma_1 & \ldots & \Sigma_{p-1} \\
  \Sigma_1^{T} & \Sigma _0 & \Sigma 1 & \ldots \\
  \vdots & \ddots & \ddots & \ldots \\
  \Sigma_{p-1}^T & \Sigma_{p-2}^T & \ldots & \Sigma_{0}
 \end{array} \right)
 \end{align}
 \begin{align}
 \tilde{\Sigma_1} = \left(\begin{array}{cccc}
 \Sigma_1 & \Sigma_2 & \ldots & \Sigma_{p} \\
 \Sigma_{0} & \Sigma_1 & \Sigma_2 & \ldots \\
 \Sigma_{1}^{T} & \Sigma_{0} & \Sigma_1 & \ldots \\
 \vdots & \ddots & \ddots & \ldots \\
 \Sigma_{p-2}^T & \Sigma_{p - 3}^T & \ldots & \Sigma_1
 \end{array} \right) 
 \end{align}
 where we have used the fact that $\Sigma_{-k} = \Sigma_{k}^T$. We may then obtain a sparse VAR model by plugging in these estimates of $\tilde{\Sigma}_0$ and $\tilde{\Sigma}_1$ into Eq. \ref{prob1}. 
  
  
\bibliography{irregular}
\bibliographystyle{unsrt}

\end{document}
 