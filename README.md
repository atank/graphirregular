# README #

### What is this repository for? ###

Code implements the EM algorithm for structural VAR model estimation from subsampled and mixed frequency data from the paper
"Identifiability and Estimation of Structural Vector Autoregressive Models for Subsampled and Mixed Frequency Time Series".

