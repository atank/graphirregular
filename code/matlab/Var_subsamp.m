function [ x ] = Var_subsamp( a,p,N,eps,samprate,k )
x = zeros(N,p);
x(1:k,1:p) = normrnd(0,eps,[k p]);
for i = (k+1):N 
    x(i,1:p) = a*vec(x(fliplr((i-k):(i-1)),1:p)') + normrnd(0,eps,[p 1]);
end
 
%%%subsample data####
for i = 1:p
    these = 1:samprate(i):N;
    setna  = setdiff(1:N,these);
    x(setna,i) = NaN;
end
end

