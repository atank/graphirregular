%%%%generate ar model%%%%
k = 3;
N = 1000;
p = 1;
eps = .1;
a = [.2 .1 .01];
arev = [.01 .1 .2];
lagcov = k;


x = zeros(N,p);
x(1:k) = normrnd(0,eps,[1 k]);
for i = (k+1):N 
   x(i) = a*x((i-k):(i-1)) + normrnd(0,eps);
end

%%%%create covariances%%%%%
covc = zeros(lagcov+1,1);
covc(1) = cov(x);
for c = 1:lagcov
    fart = cov(x((c + 1):end),x(1:(end - c)));
    covc(c+1) = fart(1,2);
end

%%%%%%%%%%work baby work%%%%%
delta = .01;
theta = -.5:delta:.5;
theta = theta(2:end);
j = [0 2];
Z = (1/length(theta))*complex(cos(2*pi*j'*theta), sin(2*pi*j'*theta));
cvx_begin
    variable s(length(theta))
    minimize( -sum(log(s)))
    subject to
        Z * s == covc(j+1);
cvx_end

strue = zeros(length(theta),1)
for si = 1:length(theta)
    strue(si) = 1/(1 - sum(complex(cos(2*pi*theta(si)*(1:k),sin(2*pi*theta(si)*(1:k)
end

plot(s)

