function [ Cov ] = covariance_mat(s,k,p,theta)
Cov = zeros(p,p,k+1);
j = 0:k;
Z = (1/length(theta))*complex(cos(2*pi*j'*theta), sin(2*pi*j'*theta));
for l = 1:(k+1)
    for i = 1:p
        for j = 1:p
            Cov(i,j,kk) = Re(squeeze(s(i,j,:))'*Z(l,:)');
        end
    end
end
end

function [ Aret ] = Yule_Walker(Cov,p,k)
%%%%construct
bC = zeros(p*(k+1),p*(k+1));
bC1 = zeros(p*k,p*k);
%%%first elongate the Cov matrix
Cove = zeros(p,p*(k+1));
for i = 1:(k+1)
    ind = (i-1)*p + 1;
    Cove(:,ind:(ind+p)) = Cov(:,:,i);
end

for i = 1:(k+1)
    for j = 1:(k+1)
        xind = (i - 1)*p + 1;
        yind = (j - 1)*p + 1;
        diff = abs(i - j);
        if (i > j)
            bC(xind:(xind + p),yind:(yind + p)) = Cov(:,:,diff)';
        else 
            bC(xind:(xind + p),yind:(yind + p)) = Cov(:,:,diff);
        end
    end
end
bC0 = bC(1:(p*k),1:(p*k));
bC1 = bC(1:(p*k),(p + 1):(p*(k+1)));
%%%compute inverse
A = solve(bC0,bC1)';
Aret = A(:,1:p);
end

function [A] = spectral_factorization(s,k,p,theta)
    Cov = covariance_mat(s,k,p,theta);  
    A = Yule_Walker(Cov,p,k);
end



