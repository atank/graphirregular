function [relerr,esterr] = psim(p,w_af,a_af,eps,samprate)
A = zeros(p);
A(1,1) = a_af;
for i = 2:p
    A(i,i) = a_af;
    A(1,i) = w_af;
    A(i,1) = w_af;
end
q = max(abs(eig(A)));
A = A./(q*1.01);
ratio = w_af/a_af;

%samprate = [2 ones(p-1,1)'];

[relerr,esterr] = p_sim_maxent(A,p,2,20,20,ratio,eps,samprate);
end

