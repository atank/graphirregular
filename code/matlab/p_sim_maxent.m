%setpath('/Users/alextank/Documents/GraphIrregular/code/matlab')

function [mtot,est_err] = p_sim_maxent(a,p,k,lagcov,lagcov_constraint,ratio,eps,samprate)

%samprate = [2 1 1];
%k = 2;
%lagcov=20;
%lagcov_constraint = 20;
kest=lagcov_constraint;
%N = 400000;
%p = 3;
%eps = .2;
%a = [.9 .09 .09;.09 .9 0; .09 0 .9]/1.3;

%a = [.9 .09;.09 .9];
%p=2;
%eps=.2;


%a = [ap ap];
%a = [zeros(p) ap];
%a = [ap zeros(p)];

%mincorr = 300;
%maxcov=2;
%a = ap;
a = [a zeros(p)];

%%%%%compute theoretical covariance values
Ikp2 = eye((k*p)^2);
Ip = eye(p);
bA = [a; Ip zeros(p)];
sigu = zeros(k*p);
sigu(1:p,1:p) = eye(p)*eps^2;
covs= inv(Ikp2 - kron(bA,bA))*(sigu(:));
covf = reshape(covs',[k*p, k*p]);
lagval = zeros(p,p,lagcov+1);
lagval(:,:,1) = covf(1:p,1:p);
lagval(:,:,2) = covf(1:p,(p+1):(2*p));
if (lagcov > 2)
    for i = 3:(lagcov+1) 
        lagval(:,:,i) = a(:,1:p)*lagval(:,:,i-1) + a(:,(p+1):(2*p))*lagval(:,:,i-2);
    end
end
        

a_array = zeros(p,p,k);
a_array(:,:,1) = a(:,1:p);
if (k > 1)
    a_array(:,:,2) = a(:,(p+1):(2*p));
end

%lagcov = k;


%%%%%sample data and form covariance matrices%%%%%
%x = Var_subsamp(a,p,N,eps,samprate,k);



%[lagval, lagav] = compute_covariances(x,maxcov,mincorr,p);


%%%%optimization problem
%%%%remove the ones not available%%%%
lagav = ones(p,p,lagcov+1);

for i = 1:p
    for l = 2:(lagcov + 1)
        if (samprate(i) > 1)
            if ~(mod((l - 1),samprate(i)) == 0)
                lagav(i,i,l) = 0;
            end
        end
    end
end

for i = 1:p
    for j = (i + 1):p
        for l = 2:(lagcov + 1)
            mins = min(samprate(i),samprate(j));
            if (mins > 1)
                if ~(mod((l - 1),mins) == 0)
                  lagav(i,j,l) = 0;
                  lagav(j,i,l) = 0;
                end
            end
        end
    end
end

lagav
%lagav = ones(p,p,lagcov+1);
%lagav(:,:,2)=0;
%lagav(2,2,2)=0;
%lagav(2,2,3)=0;

%lagav = ones(p,p,lagcov+1);
%lagav(2,2,2)=0;

delta = .01;
theta = -.5:delta:.5;
theta = theta(2:end);
j = 0:lagcov;
Z = (1/length(theta))*complex(cos(2*pi*j'*theta), sin(2*pi*j'*theta));

dir = length(theta);
cvx_begin
    variable s(p,p,dir) hermitian semidefinite
    expression x(dir)
    sd = conj(s);
    for i = 1:dir
        x(i) = -log_det(s(:,:,i));
    end
    %minimize(x(1) + x(2))
    minimize(sum(x))
    subject to
        for l = 1:(lagcov_constraint+1)
            for i = 1:p
                for j = 1:p
                    %if ~(l == 1 && i == 1 && j == 2)
                    if ~(l == 1 && j > i)
                        if lagav(i,j,l)
                            squeeze(s(i,j,:))'*Z(l,:)'== complex(lagval(i,j,l),0);
                     %squeeze(s(i,j,:))'*Z(l,:)'== complex(lagval(i,j,l),0);
                     %squeeze(sd(i,j,:))'*Z(l,:)'== complex(lagval(j,i,l),0);  
                     %if (i ~= j)
                     %     squeeze(conj(s(i,j,:)))'*Z(l,:)'== complex(lagval(i,j,l),0);
                     %end
                        end
                    end
                end
            end
        end
cvx_end


%%%%%recover lagged covariance matrix, and perform Yule-Walker estimation
%%%%%to recover the autoregressive matrices###############################
[A1,A,covs] = spectral_factorization(s,kest,p,theta);

%%%select VAR(1) processes
A1est = A(1:2,1:2);
est_err = norm(A1est - a(1:2,1:2),'fro');

%%%%%recover covariance matrices up to some depth and compare with true values
covv = covariance_mat(s,lagcov,p,theta);

%%%%%%%plot imputed autocovariance estimates%%%%%%%
i = 1;
j = 1;
h1 = figure();set(gcf,'Visible', 'off'); 
plot(squeeze(covv(i,j,:)),'b--o'); hold on
plot(squeeze(lagval(i,j,:)),'g--o')
filename = strcat('res/',num2str(i),num2str(j),'cov',num2str(p),'_',num2str(ratio),'_',num2str(samprate(2)),'.png');
print(filename,'-dpng');
close;
%%%compute relative distance between covariance sequence
tot = 0;
for i = 1:(lagcov + 1)
    tot = tot + abs(covv(1,1,i) - lagval(1,1,i))/(abs(lagval(1,1,i)));
end
mtot = tot/(lagcov + 1);
end
