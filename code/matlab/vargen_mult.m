%setpath('/Users/alextank/Documents/GraphIrregular/code/matlab')


k = 2;
kest=1;
lagcov=20;
N = 400000;
p = 2;
eps = .2;
ap = [.4 0;.1 .4];

a = [ap ap];
a = [ap zeros(p)];
mincorr = 300;
maxcov=2;
%a = ap;


%%%%%compute theoretical covariance values
Ikp2 = eye((k*p)^2);
Ip = eye(p);
bA = [a; Ip zeros(p)]
sigu = zeros(k*p);
sigu(1:p,1:p) = eye(p)*eps^2;
sigu

covs= inv(Ikp2 - kron(bA,bA))*sigu(:);
covf = reshape(covs',[k*p, k*p]);
lagval = zeros(p,p,lagcov+1);
lagval(:,:,1) = covf(1:p,1:p);
lagval(:,:,2) = covf(1:p,(p+1):(2*p));
if (lagcov > 2)
    for i = 3:(lagcov+1) 
        lagval(:,:,i) = a(:,1:p)*lagval(:,:,i-1) + a(:,(p+1):(2*p))*lagval(:,:,i-2);
    end
end
        

a_array = zeros(p,p,k);
a_array(:,:,1) = a(:,1:p);
if (k > 1)
    a_array(:,:,2) = a(:,(p+1):(2*p));
end

%lagcov = k;
samprate = [1 2];


%%%%%sample data and form covariance matrices%%%%%
%x = Var_subsamp(a,p,N,eps,samprate,k);



%[lagval, lagav] = compute_covariances(x,maxcov,mincorr,p);


%%%%optimization problem
%%%%remove the ones not available%%%%
lagav = ones(p,p,lagcov+1);

for i = 1:p
    for l = 2:(lagcov + 1)
        if (samprate(i) > 1)
            if ~(mod((l - 1),samprate(i)) == 0)
                lagav(i,i,l) = 0;
            end
        end
    end
end

for i = 1:p
    for j = (i + 1):p
        for l = 2:(lagcov + 1)
            mins = min(samprate(i),samprate(j));
            if (mins > 1)
                if ~(mod((l - 1),mins) == 0)
                  lagav(i,j,l) = 0;
                  lagav(j,i,l) = 0;
                end
            end
        end
    end
end


%lagav = ones(p,p,lagcov+1);
%lagav(2,2,2)=0;

delta = .01;
theta = -.5:delta:.5;
theta = theta(2:end);
j = 0:lagcov;
Z = (1/length(theta))*complex(cos(2*pi*j'*theta), sin(2*pi*j'*theta));

dir = length(theta);
cvx_begin
    variable s(p,p,dir) hermitian semidefinite
    expression x(dir)
    sd = conj(s);
    for i = 1:dir
        x(i) = -log_det(s(:,:,i));
    end
    %minimize(x(1) + x(2))
    minimize(sum(x))
    subject to
        for l = 1:(lagcov+1)
            for i = 1:p
                for j = 1:p
                    if ~(l == 1 && i == 1 && j == 2)
                        if lagav(i,j,l)
                            squeeze(s(i,j,:))'*Z(l,:)'== complex(lagval(i,j,l),0);
                     %squeeze(s(i,j,:))'*Z(l,:)'== complex(lagval(i,j,l),0);
                     %squeeze(sd(i,j,:))'*Z(l,:)'== complex(lagval(j,i,l),0);  
                     %if (i ~= j)
                     %     squeeze(conj(s(i,j,:)))'*Z(l,:)'== complex(lagval(i,j,l),0);
                     %end
                        end
                    end
                end
            end
        end
cvx_end


%%%%%recover lagged covariance matrix, and perform Yule-Walker estimation
%%%%%to recover the autoregressive matrices###############################
[A,covs] = spectral_factorization(s,kest,p,theta);
A'


%%%%%recover covariance matrices up to some depth and compare with true values
covv = covariance_mat(s,lagcov,p,theta)



%%%%%%%plot imputed autocovariance estimates%%%%%%%
for i = 1:p
	for j = 1:p
		plot(squeeze(covv(i,j,:)),'b--o'); hold on
		plot(squeeze(lagval(i,j,:)),'g--o')
	end
end



%%%%%compute true spectral density matrix%%%%%
Alam = zeros(p,p,length(theta));
for i = 1:p
    for j = 1:p
        for si = 1:length(theta)
            Alam(i,j,si) = sum(complex(squeeze(a_array(i,j,:))'.*cos(2*pi*theta(si)*(1:k)),squeeze(a_array(i,j,:))'.*sin(2*pi*theta(si)*(1:k))));
        end
    end
end
Sig = eps^2*eye(p);
Id = eye(p);
S_true = zeros(p,p,length(theta));
for si = 1:length(theta)
   S_true(:,:,si) = inv(Id - Alam(:,:,si)) * Sig * inv(Id - Alam(:,:,si))';
end

%%%%make some plots%%%%

plot(squeeze(real(S_true(1,1,:)))); hold on;
plot(squeeze(real(s(1,1,:))),'g');
print('var2_12_1','-dpng')


plot(squeeze(real(S_true(2,2,:)))); hold on;
plot(squeeze(real(s(2,2,:))),'g');
print('var2_12_2','-dpng')



plot(squeeze(abs(S_true(2,1,:)))); hold on;
plot(squeeze(abs(s(2,1,:))),'g');
print('var2_12_12','-dpng')


















