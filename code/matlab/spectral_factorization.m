function [A1,A,Cov] = spectral_factorization(s,kmax,p,theta)
    Cov = covariance_mat(s,kmax,p,theta);
    A = Yule_Walker(Cov,p,kmax);
    A1 = A1_project(A',Cov,p,kmax);
end
