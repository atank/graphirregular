function [ Cov ] = covariance_mat(s,k,p,theta)
Cov = zeros(p,p,k+1);
j = 0:k;
Z = (1/length(theta))*complex(cos(2*pi*j'*theta), sin(2*pi*j'*theta));
for l = 1:(k+1)
    for i = 1:p
        for j = 1:p
            Cov(i,j,l) = real(squeeze(s(i,j,:))'*Z(l,:)');
        end
    end
end
end
