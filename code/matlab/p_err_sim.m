
w_afv = [.5 .4 .3 .2 .1 0];

for kk = 1:length(w_afv)
    w_af = w_afv(kk);
    a_af = 1 - w_af;
    %pmax = 2;
    pmax = 7;
    relerr1 = zeros(pmax-1,1);
    relerr2 = zeros(pmax-1,1);
    esterr1 = zeros(pmax-1,1);
    esterr2 = zeros(pmax-1,1);
    %w_af = .1;
    %a_af = .9;
    eps=.2;
    for i = 2:pmax 
        samprate1 = [2 ones(i-1,1)'];
        [relerr1(i-1),esterr1(i-1)] = psim(i,w_af,a_af,eps,samprate1);
        %samprate2 = [2 2*ones(i-1,1)'];
        %[relerr2(i-1),esterr2(i-1)] = psim(i,w_af,a_af,eps,samprate2);
    end

    err = [relerr1;relerr2;esterr1;esterr2];
    datf = strcat('res/',num2str(a_af),'est_res.mat');
    save(datf,'err');

    indmax = pmax-1;

    h1 = figure();set(gcf,'Visible', 'off'); 
    plot(2:pmax,relerr1(1:indmax));hold on
    %plot(2:pmax,relerr2(1:indmax))
    xlabel('p')
    ylabel('relative error');
    fle = strcat('res/',num2str(a_af),'interp_error_scale');

    print(strcat(fle,'.png'),'-dpng');
    close();

    h2 = figure();set(gcf,'Visible', 'off'); 
    plot(2:pmax,esterr1(1:indmax));hold on
    %plot(2:pmax,esterr2(1:indmax))
    xlabel('p')
    ylabel('relative error')
    fle2 = strcat(fle,'_A.png');
    print(fle2,'-dpng')
    close();
end
