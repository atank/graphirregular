function [lagval, lagav] = compute_covariances(x,maxcov,mincorr,p)
lagav = zeros(p,p,maxcov+1);
lagval = zeros(p,p,maxcov+1);

%%compute cross covariances###
for l = 0:maxcov
    for i = 1:p
        for j = 1:p
            %cov(x((l+1):end,i),x(1:(end - l),j))
            prod = x((l+1):end,i).*x(1:(end - l),j);
            prods = ~isnan(prod);
            Ts = sum(prods);
            if (Ts > mincorr)
                lagav(i,j,l+1) = 1;
                lagval(i,j,l+1) = sum(prod(prods==1))/(Ts);
            end
        end
    end
end