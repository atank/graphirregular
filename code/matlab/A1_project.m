function [A1] = A1_project(A,covs,p,kmax)
sum = zeros(p);
for i = 1:2
    ind = (i - 1)*p + 1;
    sum = sum + A(1:p,ind:(ind + p - 1)) * covs(1:p,1:p,i)';
end
A1 = covs(1:p,1:p,1)\sum';
A1 = A1';
