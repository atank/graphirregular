function [ Aret ] = Yule_Walker(Cov,p,k)
%%%%construct
bC = zeros(p*(k+1),p*(k+1));
bC1 = zeros(p*k,p*k);
%%%first elongate the Cov matrix

for i = 1:(k+1)
    for j = 1:(k+1)
        xind = (i - 1)*p + 1;
        yind = (j - 1)*p + 1;
        diff = abs(i - j) + 1;
        if (i > j)
            bC(xind:(xind + p - 1),yind:(yind + p - 1)) = Cov(:,:,diff)';
        else 
            bC(xind:(xind + p - 1),yind:(yind + p - 1)) = Cov(:,:,diff);
        end
    end
end
bC0 = bC(1:(p*k),1:(p*k));
%bC1 = bC(1:(p*k),(p + 1):(p*(k+1)));
bC1 = bC((p + 1):(p*(k+1)),1:(p*k));

%%%compute inverse
Aret = (bC0 \ bC1);
%Aret = A(:,1:p);
end