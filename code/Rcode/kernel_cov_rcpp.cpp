#include <Rcpp.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}

double kernelf_cpp(double d,int type,double h) {
  double MYPI = 3.14159265359;
  if (type == 1) {
    double h = .25;
    double x = (1.0/(sqrt(2.0*MYPI)*h))*exp(-pow(d,2)/(2.0*pow(h,2)));
    return(x);
  }
}

// [[Rcpp::export]]
List lagged_kernel_cov_rcpp(List datT,List datV,int lagest,int p,double width,int type) {
  List lagcovL(lagest + 1);
  Rcpp::List datTc(datT);
  Rcpp::List datVc(datV);
  for (int i = 0; i < (lagest + 1); i++) {
    double ii = i;
    NumericMatrix lagcov(p,p);
    for (int j = 0; j < p;j++) {
      SEXP datTj = datTc[j];
      NumericVector datTjj(datTj);
      
      SEXP datVj = datVc[j];
      NumericVector datVjj(datVj);
      
      
      int Nj = datVjj.size();
      for (int k = 0;k < p;k++) {
        SEXP datTk = datTc[k];
        NumericVector datTkk(datTk);
        
        SEXP datVk = datVc[k];
        NumericVector datVkk(datVk);
        
        int Nk = datVkk.size();
        //showValue(Nk);
        
        double sumtop = 0;
        double sumbot = 0;
        for (int m = 0; m < Nj;m++) {
          for (int n = 0;n < Nk;n++) {
            //showValue(Nj);
            //showValue(Nk);
            double kf = kernelf_cpp(datTjj[m] - datTkk[n] - ii,type,width);
            sumtop += datVjj(m)*datVkk[n]*kf;
            sumbot += kf;
            //showValue(kf);
            //showValue(datTjj[m] - datTkk[n] - ii);
          }
        } 
        lagcov(j,k) = sumtop/sumbot;
      }
    }
    lagcovL(i) = lagcov;
  }
  return(lagcovL);
}


