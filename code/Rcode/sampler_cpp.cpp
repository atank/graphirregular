#include <RcppArmadillo.h>
#include <Rcpp.h>
#include <RcppArmadilloExtensions/sample.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::export]]
void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}

// [[Rcpp::export]]
void showValue_int(int x) {
    Rcout << "The value is " << x << std::endl;
}



// [[Rcpp::depends(RcppArmadillo)]]

// [[Rcpp::export]]
void showMatrix(arma::mat X) {
    Rcout << "Armadillo matrix is" << std::endl << X << std::endl;
}

// [[Rcpp::export]]
void showMatrix_int(IntegerMatrix X) {
    Rcout << "Armadillo matrix is" << std::endl << X << std::endl;
}

// [[Rcpp::export]]
arma::mat mvrnormArma(int n, arma::vec mu, arma::mat sigma) {
   int ncols = sigma.n_cols;
   arma::mat Y = arma::randn(n, ncols);
   return arma::repmat(mu, 1, n).t() + Y * arma::chol(sigma);
}

double logsumexp(NumericVector x) {
  return(log(sum(exp(x - max(x)))) + max(x));
}

// [[Rcpp::export]]
NumericVector dnorm_cpp_vec_log(double x, NumericVector mu, NumericVector sigma) {
  NumericVector logp(mu.size());
  for (int i = 0;i<mu.size();i++) {
    logp[i] = R::dnorm(x,mu[i], sigma[i], true);
  }
  return(logp);
}

// [[Rcpp::export]]
List sample_z_cpp(arma::mat x, NumericMatrix mu, NumericMatrix sigma, arma::mat A, NumericMatrix theta, int p, int k,int lag,int N) {
  NumericVector prob(p);
  IntegerMatrix z(N,p);
  if (k == 1) {
    std::fill(z.begin(),z.end(),1);
  }
  arma::mat errors(N,p);
  errors.fill(0);
  IntegerVector inds = seq_len( k );
  for (int j = lag; j < N; j++) {
    errors.row(j) = x.row(j) - x.row(j-1)*A.t();
    if (k > 1) {
      for (int i = 0;i < p;i++) {
        NumericVector lprob = dnorm_cpp_vec_log(errors(j,i),mu.row(i),sqrt(sigma.row(i)));
        lprob = lprob + log(theta.row(i));
        lprob = lprob - logsumexp(lprob);
        NumericVector prob = exp(lprob);
        //check this to make sure not off by one
        IntegerVector q = RcppArmadillo::sample(inds,1,true,prob);
        z(j,i) = (int)q[0];
      }
    }
  }

  List output(2);
  output[0] = z;
  output[1] = errors;
  return(output);
}

NumericVector rdirichlet_cpp(Rcpp::NumericVector a) {
  NumericVector pr(a.size());
  NumericVector sample(a.size());
  double sample_sum = 0;
  for(int i=0; i<a.size(); i++) {
    sample[i] = as<double>(rgamma(1, a[i], 1));
    sample_sum += sample[i];
  }
  for(int i = 0; i<a.size(); i++) {
    pr[i] = sample[i] / sample_sum ;
  }
  return(pr);
}

// [[Rcpp::export]]
NumericMatrix sample_theta_cpp(NumericMatrix counts,double alpha,int k,int p) {
  NumericMatrix theta(p,k);
  for (int i = 0;i < p;i++) {
    if (k == 1) {
      theta(i,0) = 1;
    } else {
      NumericVector combined = counts.row(i) + alpha;
      theta.row(i) = rdirichlet_cpp(combined);
    }
  }
  return(theta);
}

// [[Rcpp::export]]
List error_stats_cpp(IntegerMatrix z, NumericMatrix errors,int p,int k, int N,int lag) {
  NumericMatrix sampmean(p,k);
  NumericMatrix ss(p,k);
  NumericMatrix counts(p,k);
  for (int i = 0;i < p;i++) {
    for (int j = lag; j < N;j++) {
      sampmean(i,z(j,i) - 1) += errors(j,i);
      counts(i,z(j,i) - 1) += 1;
    }
  }
  
  if (k == 1) {
    for (int i = 0;i < p;i++) {
      for (int j = 0; j < k;j++) {
        sampmean(i,j) = 0;
      }
    }
  }
  if (k > 1) {
    for (int i = 0;i < p;i++) {
      for (int j = 0; j < k;j++) {
        if (counts(i,j) > 0) {
          sampmean(i,j) = sampmean(i,j)/counts(i,j);
        }
      }
    }
  }
  
  
  for (int i = 0;i < p;i++) {
    for (int j = lag;j < N;j++) {
      ss(i,z(j,i) - 1) += (errors(j,i) - sampmean(i,z(j,i) - 1))*(errors(j,i) - sampmean(i,z(j,i)-1));
    }
  }

  
    
  List output(3);
  output[0] = counts;
  output[1] = sampmean;
  output[2] = ss;
  return(output);
}

// [[Rcpp::export]]
List sample_musig_cpp(NumericMatrix means, NumericMatrix ss, NumericMatrix counts, int p, int k,double beta, double alpha, double lambda) {
  NumericMatrix sigma(p,k);
  NumericMatrix mu(p,k);
  for (int i = 0; i < p; i++) {
    if (k == 1) {
      mu(i,0) = 0;
      double alpha_star = alpha + .5*counts(i,0);
      double beta_star = beta + .5*(ss(i,0));
      double tau = R::rgamma(alpha_star,1.0/beta_star);
      sigma(i,0) = 1/tau;
    }
    if (k > 1) {
      for (int j = 0;j < k; j++) {
        double lambda_star = lambda + counts(i,j);
        double alpha_star = alpha + counts(i,j)/2;
        double beta_star = beta + (.5)*(ss(i,j) + (lambda*counts(i,j)*means(i,j)*means(i,j))/(lambda + counts(i,j)));
        double mean_star = (counts(i,j)*means(i,j))/(lambda + counts(i,j));
        double tau = R::rgamma(alpha_star,1.0/beta_star);
        sigma(i,j) = 1/tau;
        mu(i,j) = R::rnorm(mean_star,sqrt(1/(lambda_star*tau)));
      }
    }
  }
  List output(2);
  output[0] = mu;
  output[1] = sigma;
  return(output);
}


IntegerVector which_zero(IntegerVector gamma) {
  int p = gamma.size();
  IntegerVector idx = seq(0,p-1);
  return(idx[gamma != 0]);
}

IntegerMatrix sample_gamma_cpp(IntegerMatrix z, arma::mat x, arma::mat mupp, arma::mat sigmapp, int p, int k, int lag, int N, double sigma_A,IntegerMatrix gamma,double phi) {
  arma::mat mup = mupp.cols(1,N-1);
  arma::mat sigmap = sigmapp.cols(1,N-1);
  arma::mat X = x.rows(0,N-2);
  arma::mat Xq = x.rows(0,N-2);
  arma::mat Y = x.rows(1,N-1);
  arma::mat eyep(p,p);
  eyep.eye();
  IntegerVector inds = seq_len( 2 ) - 1;
  for (int i = 0; i < p; i++) {
    //compute the dot product, to be used in each of the below
    arma::mat Xsiginv = X.each_col() / sigmap.row(i).t();
    arma::mat K = X.t() * Xsiginv + (1.0/sigma_A)*eyep; 
    arma::colvec y = Y.col(i) - mup.row(i).t();
    arma::rowvec s = (y.t()/sigmap.row(i));
    double log_p_base =  -.5*sum(log(sigmap.row(i))) - (((double)(N-1.0))/(2.0))*log(2*M_PI) - .5*(dot(s,y));
    //cycle through each predictor, sampling
    for (int j = 0;j < p; j++) {
      //showValue_int(j);
      NumericVector lprob(2);
      
      IntegerVector gammai = gamma.row(i);
      arma::rowvec gam=as<arma::rowvec>(gammai);
      //gam.print();
      gammai(j) = 1;
      IntegerVector active_j = which_zero(gammai);
      double n_activej = (double)active_j.size();
      arma::mat K_j = K.submat(as<arma::uvec>(active_j),as<arma::uvec>(active_j));
      arma::mat Xsiginv_j = Xsiginv.cols(as<arma::uvec>(active_j));
      arma::vec m_j = inv(K_j) * Xsiginv_j.t() * y;
      lprob(1) = log_p_base  - .5*log(det(K_j)) + dot(m_j.t()*K_j, m_j) + log(phi) - .5*n_activej*log(sigma_A);
      
      //m_j.print();
      //K_j.print();
      
      
      gammai(j) = 0;
      IntegerVector active_noj = which_zero(gammai);
      double n_noactivej = (double)active_noj.size();
      arma::mat K_noj = K.submat(as<arma::uvec>(active_noj),as<arma::uvec>(active_noj));
      arma::mat Xsiginv_noj = Xsiginv.cols(as<arma::uvec>(active_noj));
      arma::vec m_noj = inv(K_noj) * Xsiginv_noj.t() * y;
      lprob(0) = log_p_base  - .5*log(det(K_noj)) + dot(m_noj.t()*K_noj, m_noj) + log(1.0 - phi) - .5*n_noactivej*log(sigma_A);
      //m_noj.print();
      //K_noj.print();
      
      
      lprob = lprob - logsumexp(lprob);
      //arma::rowvec sdf = as<arma::rowvec>(lprob);
      //arma::rowvec prob = exp(lprob);
      //prob.print();
      
      IntegerVector q = RcppArmadillo::sample(inds,1,true,exp(lprob));
      //showValue_int((int)q(0));
      gamma(i,j) = (int)q(0);
    }
  }
  return(gamma);
}
  
  
  //now sample A given gamma
  // [[Rcpp::export]]
  arma::mat sample_A_normal_prior_cpp_wgamma(IntegerMatrix z, arma::mat x, NumericMatrix mu, NumericMatrix sigma, int p, int k,int lag, int N,double sigma_A, IntegerMatrix gamma) {
    arma::mat A(p,p);
    arma::mat d(p,p);
    d.eye();
    for (int i = 0; i < p; i++) {
      IntegerVector gammai = gamma.row(i);
      IntegerVector activei = which_zero(gammai);
      arma::mat x_activei = x.cols(as<arma::uvec>(activei));
      
      arma::rowvec summu(activei.size());
      summu.fill(0);
      arma::mat sumsig(activei.size(),activei.size());
      sumsig.fill(0);
      for (int j = lag; j < N; j++) {
        summu += x_activei.row(j-1)*(x(j,i) - mu(i,z(j,i) - 1))/(sigma(i,z(j,i) - 1));
        sumsig += x_activei.row(j-1).t()*x_activei.row(j-1)/sigma(i,z(j,i)-1);
        //sumsig.print();
      }
      
      arma::mat sigma_star = inv(sumsig + (1.0/sigma_A)*d.submat(as<arma::uvec>(activei),as<arma::uvec>(activei)));
      //sumsig.print();
      //sigma_star.print();
      arma::colvec mu_star = sigma_star * summu.t();
      
      arma::rowvec arowi(p);
      arowi.fill(0);
      arowi(as<arma::uvec>(activei)) = mvrnormArma(1,mu_star,sigma_star);
      A.row(i) = arowi;
    }
    return(A);
  }


// [[Rcpp::export]]
arma::mat sample_A_normal_prior_cpp(IntegerMatrix z, arma::mat x, NumericMatrix mu, NumericMatrix sigma, int p, int k,int lag, int N,double sigma_A) {
  arma::mat A(p,p);
  arma::mat d(p,p);
  d.eye();
  for (int i = 0; i < p; i++) {
    arma::rowvec summu(p);
    summu.fill(0);
    arma::mat sumsig(p,p);
    sumsig.fill(0);
    for (int j = lag; j < N; j++) {
      summu += x.row(j-1)*(x(j,i) - mu(i,z(j,i) - 1))/(sigma(i,z(j,i) - 1));
      sumsig += x.row(j-1).t()*x.row(j-1)/sigma(i,z(j,i)-1);
      //sumsig.print();
    }
    
    arma::mat sigma_star = inv(sumsig + (1.0/sigma_A)*d);
    //sumsig.print();
    //sigma_star.print();
    arma::colvec mu_star = sigma_star * summu.t();
    A.row(i) = mvrnormArma(1,mu_star,sigma_star);
  }
  return(A);
}

// [[Rcpp::export]]
arma::mat sample_state_forward_backward_cpp(IntegerMatrix z, NumericMatrix mu,NumericMatrix sigma,arma::mat A,List Xsqueezed,List param) {
  int p = param["p"];
  int lag = param["lag"];
  arma::mat P0 = param["P0"];
  int N = param["N"];
  List active = param["active"];
  List not_active = param["not_active"];
  List C = param["C"];
  arma::mat fm(p,N);
  arma::mat fmout(N,p);
  arma::mat smout(N,p);
  arma::cube fc(p,p,N);
  arma::mat mup(p,N);
  arma::cube covp(p,p,N);
  covp.fill(0);
  fm.col(lag-1).fill(0);
  fc.slice(lag-1) = P0;
  arma::mat eyep(p,p);
  eyep.eye();
  for (int i = lag; i < N; i++) {
    //showValue_int(i);
    arma::mat Ci = as<arma::mat>(C[i]);
    for (int j = 0; j < p; j++) {
      mup(j,i) = mu(j,z(i,j) - 1);
      covp(j,j,i) = sigma(j,z(i,j)-1);
    }
    
    //arma::colvec predex = A * fm[i-1] + mui;

    arma::colvec predex = A * fm.col(i-1) + mup.col(i);
    arma::mat predcov = A*fc.slice(i-1)*A.t() + covp.slice(i);

    
    arma::colvec ytilde = as<arma::colvec>(Xsqueezed[i]) - Ci*predex;
    arma::mat S = Ci*predcov*Ci.t();

    arma::mat K = predcov*Ci.t()*inv(S);
    fm.col(i) = predex + K*ytilde;
    //fmout.row(i) = fmi.t();
    fc.slice(i) = (eyep - K*Ci)*predcov;
  }
  
  ///backwards sampling
  IntegerVector ii(1);
  ii(0) = N-1;
  arma::uvec hj = as<arma::uvec>(ii);
  arma::uvec not_observed = as<arma::uvec>(active[N-1]) - 1;
  arma::uvec observed = as<arma::uvec>(not_active[N-1]) - 1;
  arma::mat Covsim = fc.slice(N-1).submat(not_observed,not_observed);
  arma::colvec musim = fm(not_observed,hj);

  arma::mat x(p,N);
  x.fill(0);
  x(not_observed,hj) = mvrnormArma(1,musim,Covsim).t();
  x(observed,hj) = fm(observed,hj);
  for (int i = N-2; i > lag;i--) {
    IntegerVector ii(1);
    ii(0) = i;
    arma::uvec hj = as<arma::uvec>(ii);
    arma::uvec not_observed = as<arma::uvec>(active[i]) - 1;
    arma::uvec observed = as<arma::uvec>(not_active[i]) - 1;
    arma::mat fc_not_observed = fc.slice(i).submat(not_observed,not_observed);
    arma::colvec fmi_not_observed = fm.submat(not_observed,hj);
    arma::colvec fmi_observed = fm(observed,hj);
    arma::mat A_not_observed = A.cols(not_observed);
    arma::mat A_observed = A.cols(observed);
    if (observed.size() == p) {
      x.col(i) = as<arma::colvec>(Xsqueezed[i]);
    } else {
      arma::mat likelihoodcov = A_not_observed.t()*inv(covp.slice(i+1))*A_not_observed;
      arma::mat Covsim = inv(likelihoodcov + inv(fc_not_observed));
      arma::vec musim = Covsim * (A_not_observed.t() * inv(covp.slice(i+1)) * (x.col(i+1) - A_observed*fmi_observed - mup.col(i+1)) + inv(fc_not_observed)*fmi_not_observed);
      x(not_observed,hj) = mvrnormArma(1,musim,Covsim).t();
      x(observed,hj) = fmi_observed;
    }
  }
  return(x.t());
}

List compute_sigma_mu_z_cpp(NumericMatrix mu,NumericMatrix sigma,IntegerMatrix z,int p, int N,int lag) {
  NumericMatrix mup(p,N);
  NumericMatrix covp(p,N);
  
  for (int i = lag; i < N; i++) {
    //showValue_int(i);
    for (int j = 0; j < p; j++) {
      mup(j,i) = mu(j,z(i,j) - 1);
      //covp(j,i) = (double)1.0/sigma(j,z(i,j)-1);
      covp(j,i) = sigma(j,z(i,j)-1);
    }
  }
  List output(2);
  output[0] = mup;
  output[1] = covp;
  return(output);
}


// [[Rcpp::export]]
List sampler_cpp(arma::mat A,arma::mat x,NumericMatrix mu,NumericMatrix sigma, NumericMatrix theta, List param,int Nsamp,int Nburnin,List Xsqueezed,IntegerMatrix gamma) {
  int p = (int)param["p"];
  int k = (int)param["k"];
  int lag = (int)param["lag"];
  int N = (int)param["N"];
  int spikenslab = (int)param["spikenslab"];
  double alpha = (double)param["alpha"];
  double sigma_A = (double)param["sigma_A"];
  double alpha_gamma = (double)param["alpha_gamma"];
  double phi = (double)param["phi"];
  double lambda = (double)param["lambda"];
  showValue(1);
  //double beta = param["beta"];
  double beta = 1;
  
  int total = Nburnin + Nsamp;
  
  IntegerMatrix z(N,p);

  NumericMatrix errors(N,p);
  NumericMatrix counts(p,k);
  NumericMatrix means(p,k);
  NumericMatrix ss(p,k);
  
  arma::cube samples_mu(p,k,Nsamp);
  arma::cube samples_sigma(p,k,Nsamp);
  arma::cube samples_theta(p,k,Nsamp);
  arma::cube samples_A(p,p,Nsamp);
  arma::cube samples_x(N,p,Nsamp);
  arma::cube samples_gamma(p,p,Nsamp);
  
  for (int i = 0; i < total;i++) {
    List outputz = sample_z_cpp(x,mu,sigma,A,theta,p,k,lag,N);
    z = as<IntegerMatrix>(outputz[0]);
    errors = as<NumericMatrix>(outputz[1]);
    
    List outpute = error_stats_cpp(z,errors,p,k,N,lag);
    counts = as<NumericMatrix>(outpute[0]);
    means = as<NumericMatrix>(outpute[1]);
    ss = as<NumericMatrix>(outpute[2]);
    
    theta = sample_theta_cpp(counts,alpha,k, p);
    

    
    List outputmusig = sample_musig_cpp(means,ss,counts,p,k,beta,alpha_gamma,lambda);
    mu = as<NumericMatrix>(outputmusig[0]);
    sigma = as<NumericMatrix>(outputmusig[1]);
    
    //if ()
    List output_sigmu_z = compute_sigma_mu_z_cpp(mu,sigma,z,p,N,lag);
    arma::mat sigp = as<arma::mat>(output_sigmu_z[0]);
    arma::mat mup = as<arma::mat>(output_sigmu_z[1]);
    
    
    
    if (spikenslab == 1) {
      gamma = sample_gamma_cpp(z,x,sigp,mup,p,k,lag,N,sigma_A,gamma,phi);
    }
    A = sample_A_normal_prior_cpp_wgamma(z,x,mu,sigma,p,k,lag,N,sigma_A,gamma);
    
    //A = sample_A_normal_prior_cpp(z,x,mu,sigma,p,k,lag,N,sigma_A);
    x = sample_state_forward_backward_cpp(z,mu,sigma,A,Xsqueezed,param);
        
    showValue_int(i);

    if (i >= Nburnin) {
      int ind = i - Nburnin;
      samples_mu.slice(ind) = as<arma::mat>(mu);
      samples_sigma.slice(ind) = as<arma::mat>(sigma);
      samples_theta.slice(ind) = as<arma::mat>(theta);
      samples_A.slice(ind) = A;
      //samples_x.slice(ind) = x;
      samples_gamma.slice(ind) = as<arma::mat>(gamma);
    }
    
  }
  return Rcpp::List::create(Rcpp::Named("samples_mu") = samples_mu,
                            Rcpp::Named("samples_sigma") = samples_sigma,
                            Rcpp::Named("z") = z,
                            Rcpp::Named("samples_theta") = samples_theta,
                            Rcpp::Named("samples_A") = samples_A,
                            Rcpp::Named("samples_x") = samples_x,
                            Rcpp::Named("samples_gamma") = samples_gamma);
  
}


