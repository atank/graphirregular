
sink("batch_runner")
#base = "mosrun -b -e -q -J6 Rscript sim_runner.R"
base = "Rscript simulation_script.R"
N = c(500,1000,3000)
k = c(1,2)
reps = 20
sd_errors = c(.25)
A_type = c(2)
sampling_type = c(1,2,3)
p = 2
alpha = 100
mu_sig = 200
shape = .9
rate = .01
spikenslab = 0

for (n in N) {
for (sd in sd_errors) {
for (m in 1:reps) {
  for (a in A_type) {
      for (s in sampling_type) {
        for (kk in k) {
          for (sp in spikenslab) {
          lg = paste('logs/',m,kk,n,sd,sp,s,a,p,alpha,mu_sig,shape,rate,'.log',sep="_")
          end = paste(base,m,kk,n,sd,sp,s,a,p,alpha,mu_sig,shape,rate,'>', sep=" ")
          end2 = paste(end,lg," 2>&1",sep="")
          cat(end2)
          cat("\n")
        }
      }
  }
}
}
}
}
sink()
