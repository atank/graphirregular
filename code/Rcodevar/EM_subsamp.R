library('matrixStats')
library('Matrix')
library('MASS')
library('gtools')
library("Rcpp")
library("RcppArmadillo")
sourceCpp('subsamp_EM/src/EM_subsamp_cpp.cpp')

standardize_C_sig <- function(C, sigma2,mu,pi) {
  mu_n = mu;sigma2_n = sigma2;C_n = C;
  p = dim(mu)[1]
  for (j in 1:p) {
    output = moments_mixture_normal(sqrt(sigma2[j,]), mu[j,], pi[j,])#
    print(c(j,output[[2]]))
    mu_n[j,] = mu[j,]/sqrt(output[[2]])
    sigma2_n[j,] = sigma2[j,]/output[[2]]
    C_n[,j] = C[,j]*sqrt(output[[2]])
    
  }
  output = list(); output$C_n = C_n; output$sigma2_n = sigma2_n;output$mu_n = mu_n;
  return(output)
}

forward_backward_smoothing <- function(Z,param,hyperparam,base_time,Y) {
    loglik = 0
    A = param$A; C = solve(param$W); B = hyperparam$B;sub = hyperparam$sub
    p = hyperparam$p
    mut = matrix(0,sub + 1,p);mut[1,] = Y[[base_time]]
    mut_tm1 = matrix(0,sub+1,p)
    Sigmat = list(); Sigmat[[1]] = matrix(0,p,p) 
    Sigmat_tm1 = list();S = list();K = list()
    u = rep(0,p)
    D = matrix(0,p,p)
    Ex_t_t = array(0,c(p,p,sub+1))
    Ex_t_tm1 = array(0,c(p,p,sub+1))
    mut_T = matrix(0,sub+1,p)
    mut_T[1,] = Y[[base_time]]
    Sigmat_T = list()
    
    ###filtering step
    for (t in 2:(sub + 1)) {
      yt = Y[[base_time + t - 1]]
      for (i in 1:p) {
        u[i] = param$mu[i,Z[t-1,i]]
        D[i,i] = param$sigma2[i,Z[t-1,i]]
      }
      mut_tm1[t,] = A %*% mut[t - 1,] + C %*% u
      Sigmat_tm1[[t]] = A %*% Sigmat[[t - 1]] %*% t(A) + C %*% D %*% t(C)
      S[[t]] = B[[t]] %*% Sigmat_tm1[[t]] %*% t(B[[t]])
      if (length(yt) > 0) {
        loglik = loglik + lmvdnorm(yt,B[[t]] %*% mut_tm1[t,],S[[t]])
      }
      if (t < (sub + 1)) {
#         if (length(yt) > 0) {
#           print("WTF")
#           rt = yt - B[[t]] %*% mut_tm1[t,]
#           K[[t]] = Sigmat_tm1[[t]] %*% t(B[[t]]) %*% solve(S[[t]])
#           mut[t,] = mut_tm1[t,] + K[[t]] %*% rt
#           Sigmat[[t]] = Sigmat_tm1[[t]] - K[[t]] %*% B[[t]] %*% Sigmat_tm1[[t]]
#         } else {
        mut[t, ] = mut_tm1[t,]
        Sigmat[[t]] = Sigmat_tm1[[t]]
        #}
      } else {
        mut[t,] = yt
        Sigmat[[t]] = matrix(0,p,p)
        mut_T[t,] = mut[t,]
        Sigmat_T[[t]] = Sigmat[[t]]
        Ex_t_t[,,t] = mut_T[t,] %*% t(mut_T[t,])
      }
    }
    ####smoothing step
    if (sub > 1) {
      for (t in (sub):2) {
        Jt = Sigmat[[t]] %*% t(A) %*% solve(Sigmat_tm1[[t+1]])
        Sigmat_T[[t]] = Sigmat[[t]] + Jt %*% (Sigmat_T[[t+1]] - Sigmat_tm1[[t+1]]) %*% t(Jt)
        mut_T[t,] = mut[t,] + Jt %*% (mut_T[t + 1,] - mut_tm1[t + 1,])
        Ex_t_tm1[,,t+1] = Sigmat_T[[t+1]] %*% t(Jt) + mut_T[t + 1,] %*% t(mut_T[t,])
        Ex_t_t[,,t] = Sigmat_T[[t]] + mut_T[t,] %*% t(mut_T[t,])
      }
    } 
    Ex_t_tm1[,,2] = mut_T[2,] %*% t(mut_T[1,])
    Ex_t_t[,,1] =mut_T[1,] %*% t(mut_T[1,])
    return(list(Ex_t_tm1,Ex_t_t,mut_T,loglik))
}

e_step <- function(param,hyperparam,Y) {
  Nblocks = hyperparam$Nblocks; zcombs = hyperparam$zcombs;p = hyperparam$p
  k = hyperparam$k
  sub = hyperparam$sub
  Ncomb = dim(zcombs)[1]
  N = hyperparam$N
  Exz_t_t = list();Exz_t = list() ;Ez = array(0,c(p,k,N))
  mu_Avg = matrix(0,N,p)
  param$C = solve(param$W)
  log_lik_complete = 0
  for (l in 1:p) {
    Exz_t_t[[l]] = list()
    Exz_t[[l]] = list()
    for (j in 1:k) {
      Exz_t_t[[l]][[j]] = array(0,c(p,p,N))
      Exz_t[[l]][[j]] = matrix(0,N,p)
    }
  }
  full_loglik = matrix(0,Nblocks,Ncomb)
  Exz_tm1_tm1 = Exz_t_t; Exz_t_tm1 = Exz_t_t; Exz_tm1 = Exz_t
  Ex_t_t = list(); Ex_t_tm1 = list(); mut = list()
  for (i in 1:Nblocks) {
    #print(c("BLOCK",i))
    logliks = rep(0,Ncomb)
    tstart = (i-1)*sub + 1;
    tend = i*sub + 1
    #print(tstart)
    for (j in 1:Ncomb) {
      z_temp = matrix(as.numeric(zcombs[j,]),sub,p)
      #logliks[j] = logliks[j] + forward_smoothingj(z_temp,param,hyperparam,tstart,Xsqueeze)
      Es = forward_backward_smoothing(z_temp,param,hyperparam,tstart,Y)
      #Es = forward_backward_smoothing_cpp(z_temp,param,hyperparam,tstart,Y)
      #print(max(abs(Es_temp[[1]] - Es[[1]])))
      #print(max(abs(Es_temp[[4]] - Es[[4]])))
      #print(max(abs(Es_temp[[3]] - Es[[3]])))
      #print(max(abs(Es_temp[[2]] - Es[[2]])))
      logliks[j] = Es[[4]]
      logliks[j] = logliks[j] + log_probability_zs(z_temp,param$pi,hyperparam)   
      Ex_t_t[[j]] = Es[[2]]; Ex_t_tm1[[j]] = Es[[1]];mut[[j]] = Es[[3]]
      full_loglik[i,j] = logliks[j]
    }
    log_lik_complete = log_lik_complete + logSumExp(logliks)
    probs = exp(logliks - logSumExp(logliks))
    #print(cbind(probs,zcombs))
    for (t in 2:(sub + 1)) {
      for (l in 1:p) {
        for (j in 1:Ncomb) {
          ti = tstart + t - 1
          z_temp = matrix(as.numeric(zcombs[j,]),sub,p)
          #print(z_temp)
          ind = z_temp[t-1,l]
          Exz_t_t[[l]][[ind]][,,ti] = Exz_t_t[[l]][[ind]][,,ti] + probs[j]*Ex_t_t[[j]][,,t]
          Exz_tm1_tm1[[l]][[ind]][,,ti] = Exz_tm1_tm1[[l]][[ind]][,,ti] + probs[j]*Ex_t_t[[j]][,,t-1]
          Exz_t_tm1[[l]][[ind]][,,ti] = Exz_t_tm1[[l]][[ind]][,,ti] + probs[j]*Ex_t_tm1[[j]][,,t]
          Exz_t[[l]][[ind]][ti,] = Exz_t[[l]][[ind]][ti,] + probs[j]*mut[[j]][t,]
          mu_Avg[ti,l] = mu_Avg[ti,l] + probs[j]*mut[[j]][t,l]
          Exz_tm1[[l]][[ind]][ti,] = Exz_tm1[[l]][[ind]][ti,] + probs[j]*mut[[j]][t-1,]
          Ez[l,ind,ti] = Ez[l,ind,ti] + probs[j]
        }
      }
    }
  }
  output=list();output$Exz_t_t = Exz_t_t;output$Exz_tm1_tm1 = Exz_tm1_tm1;
  output$Exz_t_tm1 = Exz_t_tm1; output$Exz_t = Exz_t; output$Exz_tm1 = Exz_tm1; output$Ez = Ez
  #print(full_loglik)
  output$full_loglik = logSumExp(c(full_loglik))
  output$full_loglik = log_lik_complete
  return(output)
}

squeezeX <- function(X) {
  t = dim(X)[1]
  Xsqueeze = list()
  for (i in 1:t) {
    Xsqueeze[[i]] = as.numeric(na.omit(X[i,]))
  }
  return(Xsqueeze)
}

computeBmatrices <- function(X,hyperparam) {
  t = dim(X)[1]
  Xsqueeze = list()
  B_m = diag(rep(1,hyperparam$p))
  B = list()
  for (i in 1:t) {
    which_observed = !is.na(X[i,])
    B[[i]] = B_m[which_observed,]
  }
  return(B)
}

maxdiff <-function(ExA,ExB) {
  md_f = 0
  p = 2
  k = 2
  #print(ExB$Exz_t_t)
  for (i in 1:p) {
    for (j in 1:k) {
      #print(c(i,j))
      #print(ExA$Exz_t_t[[i]][[j]] - ExB$Exz_t_t[[i]][[j]])
      md = max(abs(ExA$Exz_t_t[[i]][[j]] - ExB$Exz_t_t[[i]][[j]]))
      md2 = max(abs(ExA$Exz_t_tm1[[i]][[j]] - ExB$Exz_t_tm1[[i]][[j]]))
      md3 = max(abs(ExA$Exz_tm1_tm1[[i]][[j]] - ExB$Exz_tm1_tm1[[i]][[j]]))
      md4 = max(abs(ExA$Exz_t[[i]][[j]] - ExB$Exz_t[[i]][[j]]))
      md5 = max(abs(ExA$Exz_tm1[[i]][[j]] - ExB$Exz_tm1[[i]][[j]]))
      #print(c(md_f,md))
      md_f = max(c(md_f,md,md2,md3,md4,md5))#,md3,md4,md5))
    }
  }
  mq = max(abs(ExA$Ez - ExB$Ez))
  mz = abs(ExA$full_log_lik - ExB$full_log_lik)
  md_f = max(c(md_f,mq,mz))
  return(md_f)
}

EM_algorithm <- function(param,hyperparam,X,Xtest=NULL,nonzeroW = NULL) {
  if (is.null(nonzeroW)) {
    hyperparam$nonzeroW = 1:(hyperparam$p*hyperparam$p)
  } else {
    hyperparam$nonzeroW = nonzeroW
  }
  maxprob = -Inf
  Xsqueeze = squeezeX(X)
  hyperparam$B = computeBmatrices(X,hyperparam)
  lprob_list = c()
  hyperparam$zcombs = as.matrix(create_zcombs(hyperparam))
  iter = 1; diff= 100; 
  #thresh = .00000001; max_iter = 400
  thresh=.000001; max_iter=100
  param$W = solve(param$C)
  alpha = 1.1; eta = 1
  diff_param = 100; thresh_param = .000001
  z_temp_list = list()
  Ncomb = dim(hyperparam$zcombs)[1]
  for (j in 1:Ncomb) {
    z_temp_list[[j]] = matrix(as.numeric(hyperparam$zcombs[j,]),hyperparam$sub,hyperparam$p)
  }
  
  while (diff > thresh & iter < max_iter) {
    #print(iter)
    param_old = param
    #print("doing E")
    
    output_Ex = e_step_cpp(param,hyperparam,Xsqueeze,z_temp_list)

    #output_Ex = e_step(param,hyperparam,Xsqueeze)
    #print(Sys.time() - t)
    #print(maxdiff(output_Ex,output_Ex2))
    
    
    #output_Ex2 = e_step_cpp(param,hyperparam,Xsqueeze,z_temp_list)
    #print("did E")
    lprob_list[iter] = output_Ex$full_loglik
    if (iter > 1){
      diff = (lprob_list[iter] - lprob_list[iter - 1])/abs(lprob_list[iter])
      #print(c("diff",diff))
      #print(diff)
      if (is.nan(diff)) {
        diff = thresh/2
        }
      if (diff < thresh) {
        #print("here")
        eta = 1
        param = param_EM
        #output_Ex = e_step(param,hyperparam,Xsqueeze)
        output_Ex = e_step_cpp(param,hyperparam,Xsqueeze,z_temp_list)
        #print(maxdiff(output_Ex,output_Ex2))
        
        lprob_list[iter] = output_Ex$full_loglik
        diff = (lprob_list[iter] - lprob_list[iter - 1])/abs(lprob_list[iter])
        if (is.nan(diff)) {
            diff = thresh/2
        }
    }
      else {
        eta = alpha*eta
      }
    }
    param_EM = M_step(param,hyperparam,output_Ex)
    #print("finished M")
    if (eta > 1 & iter > 1) {
      param = adaptive_over_relax(param_EM,param_old,eta,hyperparam)
    } else {
      param = param_EM
    }
    #print(param$W)
    #print(param$sigma2)
    diff_param = compute_difference(param,param_old)
    #print(diff_param)
    if (iter > 1) {
      #print(diff)
      #print(eta)
    }
    iter = iter + 1
  }
  #plot(lprob_list[2:length(lprob_list)])
  param_old$lprob_list = lprob_list
  if (!is.null(Xtest)) {
    Xsqueezetest = squeezeX(Xtest)
    hyperparam$B = computeBmatrices(Xtest,hyperparam)
    output_Extest = e_step_cpp(param,hyperparam,Xsqueezetest,z_temp_list)
    param_old$test_loglik = output_Extest$full_loglik
  }
  return(param_old)
}

adaptive_over_relax <- function(param,param_old,eta,hyperparam) {
  p = hyperparam$p; k = hyperparam$k
  param$A = param_old$A + eta*(param$A - param_old$A)
  param$W = param_old$W + eta*(param$W - param_old$W)
  param$mu = param_old$mu + eta*(param$mu - param_old$mu)
  ###over-relax variance parameters
  for (i in 1:p) {
    for (j in 1:k) {
     param$sigma2[i,j] = param_old$sigma2[i,j]*(param$sigma2[i,j]/param_old$sigma2[i,j])^eta 
     param$pi[i,j] = param_old$pi[i,j]*(param$pi[i,j]/param_old$pi[i,j])^eta 
    }
    param$pi[i,] = param$pi[i,]/sum(param$pi[i,]) 
  }
  return(param)
}

compute_difference <- function(param,param_old) {
  diff_A = norm(param$A - param_old$A,'f')
  diff_sigma2 = norm(param$sigma2 - param_old$sigma2,'f')
  diff_mu = norm(param$mu - param_old$mu,'f')
  diff_W = norm(param$W - param_old$W,'f')
  diff_pi = norm(param$pi - param_old$pi,'f')
  maxdiff = max(c(diff_A,diff_sigma2,diff_mu,diff_W,diff_pi))
  return(maxdiff)
} 

create_zcombs <- function(hyperparam) {
  templist = list()
  for (i in 1:(hyperparam$p*(hyperparam$sub))){
    templist[[i]] = 1:hyperparam$k
  }
  return(expand.grid(templist))
} 

log_probability_zs <- function(Z,pi,hyperparam) {
  logp = 0
  for (i in 1:hyperparam$p) {
    for (j in 1:dim(Z)[1]) {
      logp = logp + log(pi[i,Z[j,i]])
    }
  }
  return(logp)
}

M_step <- function(param,hyperparam,E) {
  for (i in 1:hyperparam$Mstep_iter) {
    if (hyperparam$opt_A) {
      #param$A = M_A(param,hyperparam,E)
      param$A = M_A_cpp(param,hyperparam,E)
      #print(param$A)
      #print(Atemp)
      #print(param$A)
    }
    if (hyperparam$opt_W) {
      #param$W = M_C(param,hyperparam,E)
      param$W = M_C_cpp(param,hyperparam,E)
      #print(param$W)
    }
    if (hyperparam$opt_pi) {
      param$pi = M_pi(param,hyperparam,E)
      #print(param$pi)
    }
    if (hyperparam$opt_mu) {
      param$mu = M_mu(param,hyperparam,E)
      #print(param$mu)
    }
    if (hyperparam$opt_sigma2) {
      #param$sigma2 = M_sig2(param,hyperparam,E) 
      param$sigma2 = M_sig2_cpp(param,hyperparam,E)
      #print(param$sigma2)
      #print(sig2temp)
    }
  }
  return(param)
}

M_pi <- function(param,hyperparam,E) {
  p = hyperparam$p; k = hyperparam$k;N = hyperparam$N
  for (i in 1:p) {
    for (j in 1:k) {
      param$pi[i,j] = sum(E$Ez[i,j,2:N])/(hyperparam$N - 1)
    }
  }
  return(param$pi)
}

M_A <- function(param,hyperparam,E) {
  W = param$W; N = hyperparam$N; p = hyperparam$p; 
  mu = param$mu; sigma2 = param$sigma2;k=hyperparam$k
  temp_mat = list()
  temp_bs = list()
  B = matrix(0,p,p)
  for (i in 1:p) {
    temp_mat = matrix(0,p,p)
    temp_bs = rep(0,p)
    for (t in 2:N) {
      for (j in 1:k) {
        temp_mat = temp_mat + E$Exz_tm1_tm1[[i]][[j]][,,t]/sigma2[i,j]
        temp_bs = temp_bs + ( - mu[i,j]*E$Exz_tm1[[i]][[j]][t,] + 
                               t(E$Exz_t_tm1[[i]][[j]][,,t]) %*% W[i,])/sigma2[i,j]
      }
    }
    B[i,] = solve(temp_mat) %*% temp_bs
  }
  A = solve(W) %*% B
  return(A)
}

M_sig2 <- function(param,hyperparam,E) {
  p = hyperparam$p; k = hyperparam$k; W = param$W; N = hyperparam$N
  mu = param$mu; A = param$A
  sigtest = param$sigma2
  start_ind = 1
  sig_thresh = .000001
  if (hyperparam$opt_W) {
    param$sigma2[,1] = 1;
    start_ind = 2;
  }
  for (i in 1:p) {
    
    for (j in start_ind:k) {
      
      param$sigma2[i,j] = 0
      for (t in 2:N) {
        param$sigma2[i,j] = param$sigma2[i,j] + t(W[i,]) %*% E$Exz_t_t[[i]][[j]][,,t] %*% W[i,] +
          t(W[i,]) %*% A %*% E$Exz_tm1_tm1[[i]][[j]][,,t] %*% t(A) %*% W[i,] +
          mu[i,j]^2*E$Ez[i,j,t] -
          2*mu[i,j] * W[i,] %*% E$Exz_t[[i]][[j]][t,] -
          2*t(W[i,]) %*% E$Exz_t_tm1[[i]][[j]][,,t] %*% t(A) %*% W[i,] +
          2*mu[i,j] * W[i,] %*% A %*% E$Exz_tm1[[i]][[j]][t,]
      }
      param$sigma2[i,j] = param$sigma2[i,j]/sum(E$Ez[i,j,2:N])
      if (param$sigma2[i,j] < sig_thresh) {
        param$sigma2[i,j] = sig_thresh
      }
    }
  }
  return(param$sigma2)
}

M_mu <- function(param,hyperparam,E) {
  p = hyperparam$p; k = hyperparam$k; W = param$W
  N = hyperparam$N; A = param$A
  for (i in 1:p) {
    for (j in 1:k) {
      param$mu[i,j] = t(W[i,]) %*% apply(E$Exz_t[[i]][[j]][2:N,],2,sum) -  sum((W[i,] %*% A) * apply(E$Exz_tm1[[i]][[j]][2:N,],2,sum))
      
      param$mu[i,j] = param$mu[i,j]/sum(E$Ez[i,j,2:N])
    }
  }
  return(param$mu)
}

M_C <- function(param,hyperparam,E) {
  A = param$A; mu = param$mu; sigma2 = param$sigma2;
  W = param$W; N = hyperparam$N; p = hyperparam$p; k = hyperparam$k
  C = solve(W); x = param$x
  thresh = .00001; diff = 100
  d = matrix(0,p,p)
  while (diff > thresh) {
    grad = matrix(0,2,2)
    
    C = solve(W)
    grad = grad + (N - 1)*t(C)
    
    perm_mat = matrix(0,p*p,p*p);
    for (i in 1:(p*p)) {
      temp = (i - 1) %% p
      temp2 = floor((i - 1)/p) + 1;
      perm_mat[i,temp*p + temp2] = 1;
    }
    hess = matrix(0,p*p,p*p)
    hess = hess - (N-1)*(perm_mat %*% kronecker(t(C),C));
    
    
    
    for (t in 2:N) {
      for (i in 1:p) {
        for (j in 1:k) {
          grad[i,] = grad[i,] + (- E$Exz_t_t[[i]][[j]][,,t] %*% W[i,] - 
                                   A %*% E$Exz_tm1_tm1[[i]][[j]][,,t] %*% t(A) %*% W[i,] +
                                   (E$Exz_t_tm1[[i]][[j]][,,t] %*% t(A) + t(E$Exz_t_tm1[[i]][[j]][,,t] %*% t(A))) %*% W[i,] +
                                   E$Exz_t[[i]][[j]][t,]*mu[i,j] - 
                                   A %*% E$Exz_tm1[[i]][[j]][t,]*mu[i,j])/sigma2[i,j]

          hess_p = (- E$Exz_t_t[[i]][[j]][,,t] - A %*% E$Exz_tm1_tm1[[i]][[j]][,,t] %*% t(A) +
           (E$Exz_t_tm1[[i]][[j]][,,t] %*% t(A) + t(E$Exz_t_tm1[[i]][[j]][,,t] %*% t(A))))/sigma2[i,j]
          d[i,i] = 1
          hess = hess + kronecker(hess_p,d)
          d[i,i] = 0
        }
      }
    }
    #hess = hess + bdiag(hess_p)
    Wold = W
    
    W = W - matrix(solve(hess) %*% as.vector(grad),p,p)
    #W = W + grad * hyperparam$W_gstep
    diff = norm(W-Wold,'f')
  }
  param$W = W

  return(param$W)
}

lmvdnorm <- function(x,mu,sigma) {
  l = -.5*log(det(sigma)) - .5*t(x - mu) %*% solve(sigma) %*% (x - mu) - (length(x)/2)*log(2*pi)
  return(l)
}


