library(sfsmisc)
library(Hmisc)
setwd("/Users/alextank/graphirregular/code/Rcodevar")
source("simulation_helper_funcs.R")

plot_eigen_error <- function(res,Ai,Ci,mi,Ni,sampling_types,Atrue,Ctrue,eig_types) {
  Ts = dim(res[[si]][[ei]]$A)[3]
  Ne = length(eig_types)
  Ns = length(sampling_types)
  Cerr = array(0,c(Ne,Ts,Ns))
  Aerr = array(0,c(Ne,Ts,Ns))
  Aerr_mean = matrix(0,c(Ne,Ns))
  Aerr_sd = matrix(0,c(Ne,Ns))
  for (si in 1:length(sampling_types)) {
    for (ei in 1:length(eig_types)) {
      for (ti in 1:Ts) {
        Aerr[ei,ti,si] = mean(abs(res[[si]][[ei]]$A[,,ti] - Atrue))
        Cerr[ei,ti,si] = sqrt((res[[si]][[ei]]$Cs_st[1,2,ti] - Ctrue[1,2])^2 + (res[[si]][[ei]]$Cs_st[2,1,ti] - Ctrue[2,1])^2)
      }
    }
  }
  print(Aerr)
  Aerr_mean = apply(Aerr,c(1,3),mean)
  print(Aerr_mean)
  Aerr_sd = apply(Aerr,c(1,3),sd)/sqrt(40)
  Cerr_mean = apply(Cerr,c(1,3),mean)
  Cerr_sd = apply(Cerr,c(1,3),sd)/sqrt(40)
  Npart = paste("T=",Ni,sep="")
# 
   titleA = paste("MSE A", Npart,sep=" ")
  jit = .001
  pdf(paste("/Users/alextank/Documents/subsamp_svar/Subsamp_KDD/A_eigen",Ai,Ci,mi,Ni,".pdf",sep=""),heigh=5,width=10)
  plot(eig_types,Aerr_mean[,1],main=titleA,type="l",col="blue",xlab="Max Eigenvalue", ylab="Avg. Abs. Error",cex.main=1.7,cex.lab=1.5,cex.axis=1.3)
  lines(eig_types,Aerr_mean[,2],type='l',col="red")
  lines(eig_types,Aerr_mean[,3],type='l',lty=2,col="blue")
  lines(eig_types,Aerr_mean[,4],type='l',lty=2,col="red")


  errbar(eig_types,Aerr_mean[,1],Aerr_mean[,1] + Aerr_sd[,1],Aerr_mean[,1] - Aerr_sd[,1],add = TRUE,col="blue",errbar.col="blue")
  errbar(eig_types + jit,Aerr_mean[,2],Aerr_mean[,2] + Aerr_sd[,2],Aerr_mean[,2] - Aerr_sd[,2],add = TRUE,col="red",errbar.col="red")
  errbar(eig_types - jit,Aerr_mean[,3],Aerr_mean[,3] + Aerr_sd[,3],Aerr_mean[,3] - Aerr_sd[,3],add = TRUE,col="blue",errbar.col="blue",lty=2)
  errbar(eig_types - 2*jit,Aerr_mean[,4],Aerr_mean[,4] + Aerr_sd[,4],Aerr_mean[,4] - Aerr_sd[,4],add = TRUE,col="red",errbar.col="red",lty=2)
  legend(.85,.5,c("k = 2","k = 3", "k = (1,2)","k = (1,3)"), lty=c(1,1,2,2),col=c("blue","red","blue","red"),cex=1.5)
  dev.off()
  
  titleC = paste("MSE C", Npart,sep=" ")
  pdf(paste("/Users/alextank/Documents/subsamp_svar/Subsamp_KDD/C_eigen",Ai,Ci,mi,Ni,".pdf",sep=""),heigh=5,width=10)
  plot(eig_types,Cerr_mean[,1],, main=titleC,type="l",col = "blue",xlab="Max Eigenvalue", ylab="Avg. Abs. Error",ylim = c(0,.4),cex.main=1.7,cex.lab=1.5,cex.axis=1.3)
  lines(eig_types,Cerr_mean[,2],type='l',col = "red")
  lines(eig_types,Cerr_mean[,2],type='l',col="red")
  lines(eig_types,Cerr_mean[,3],type='l',lty=2,col="blue")
  lines(eig_types,Cerr_mean[,4],type='l',lty=2,col="red")
  legend(.85,.4,c("k = 2","k = 3", "k = (1,2)","k = (1,3)"), lty=c(1,1,2,2),col=c("blue","red","blue","red"),cex=1.5)


  errbar(eig_types,Cerr_mean[,1],Cerr_mean[,1] + Cerr_sd[,1],Cerr_mean[,1] - Cerr_sd[,1],add = TRUE,col="blue",errbar.col="blue")
  errbar(eig_types + jit,Cerr_mean[,2],Cerr_mean[,2] + Cerr_sd[,2],Cerr_mean[,2] - Cerr_sd[,2],add = TRUE,col="red",errbar.col="red")
  errbar(eig_types - jit,Cerr_mean[,3],Cerr_mean[,3] + Cerr_sd[,3],Cerr_mean[,3] - Cerr_sd[,3],add = TRUE,col="blue",errbar.col="blue",lty=2)
  errbar(eig_types - 2*jit,Cerr_mean[,4],Cerr_mean[,4] + Cerr_sd[,4],Cerr_mean[,4] - Cerr_sd[,4],add = TRUE,col="red",errbar.col="red",lty=2)
  dev.off()

}



plot_hist_array <- function(A,Ai,Ci,mi,Ni,sirg,arg) {
  p = dim(A)[1]
  print(p)
  nsamp = dim(A)[3]
  X = matrix(0,p*p,nsamp)
  title = paste(arg,Ai,Ci,mi,Ni,si,sep="_")
  for (i in 1:p) {
    for (j in 1:p) {
      ind = (i - 1)*p + j
      X[ind,] = A[i,j,]
    }
  }
  boxplot(X,use.cols=FALSE,main=title)
}

plot_hist_array_A_C <- function(A,C,Ai,Ci,mi,Ni,si,Atrue,Ctrue,ei) {
  p = dim(A)[1]
  print(p)
  nsamp = dim(A)[3]
  X_A = matrix(0,p*p,nsamp)
  X_C = matrix(0,p,nsamp)
  Apart = paste("A",Ai,sep="")
  Cpart = paste("C",Ci,sep="")
  Npart = paste("T=",Ni,sep="")
  Spart= paste("k=",si,sep="")
  Eig_part = paste("max eigenvalue=",ei,sep="")
  
  titleA = paste(Spart,Eig_part,Npart,sep=" ")
  #titleC = paste("C",Ai,Ci,mi,Ni,si,sep="_")
  X_C[1,] = C[1,2,]
  X_C[2,] = C[2,1,]
  for (i in 1:p) {
    for (j in 1:p) {
      ind = (i - 1)*p + j
      X_A[ind,] = A[i,j,]
    }
  }
  eps = .1
  pwd_s = 2
  #par(mfrow=c(1,2))
  par(fig=c(0,.6,0,1))
  boxplot(X_A,use.cols=FALSE,main=titleA,names=c("A(1,1)","A(1,2)","A(2,1)","A(2,2)"),ylim = c(min(-.3,min(X_A)),max(1.1,max(X_A))),cex.axis=1.3,cex.main=1.7)
  lines(c(.5 + eps,1.5 - eps),c(Atrue[1,1],Atrue[1,1]),col="red",lwd=pwd_s)
  lines(c(1.5 + eps,2.5-eps),c(Atrue[1,2],Atrue[1,2]),col="red",lwd=pwd_s)
  lines(c(2.5 + eps,3.5-eps),c(Atrue[2,1],Atrue[2,1]),col="red",lwd=pwd_s)
  lines(c(3.5 + eps,4.5-eps),c(Atrue[2,2],Atrue[2,2]),col="red",lwd=pwd_s)
  
  
  
  par(fig=c(.6,1,0,1),new=T)
  boxplot(X_C,use.cols=FALSE,names=c("C(1,2)","C(2,1)"),ylim = c(min(-1,min(X_C)),max(1,max(X_C))),cex.axis=1.3,cex.main=1.7)
  lines(c(.5 + eps,1.5 - eps),c(Ctrue[1,2],Ctrue[1,2]),col="red",lwd=pwd_s)
  lines(c(1.5 + eps,2.5-eps),c(Ctrue[2,1],Ctrue[2,1]),col="red",lwd=pwd_s)
}

standardize_C <- function(C,Ctrue) {
  c1 = C[,1]
  c2 = C[,2]
  c1_a = abs(c1)
  c2_a = abs(c2)
  
  C1 = cbind(c1/c1[1],c2/c2[2])
  C2 = cbind(c2/c2[1],c1/c1[2])
  C1_err = sum(abs(C1 - Ctrue))
  C2_err = sum(abs(C2 - Ctrue))
  if (C1_err < C2_err) {
    return(C1)
  } else {return(C2)}
}

Atrue = array(0,c(2,2,1))
Atrue[,,1] = A_select(1,2)
#Atrue[,,2] = A_select(3,2)

Atrue[,,1] = Atrue[,,1]/(max(abs(eigen(Atrue[,,1])$values) + .01))
#Atrue[,,2] = Atrue[,,2]/(max(abs(eigen(Atrue[,,2])$values) + .01))


Ctrue = array(0,c(2,2,1))
#Ctrue[,,1] = C_select(1,2)
Ctrue[,,1] = C_select(2,2)

A_types = c(1);
C_types = c(2);
m_types = c(3);
sampling_types = c(3,4,2,6);
eig_types = c(1,.9,.8,.7,.6,.5)
seed_simulation_id = 1:40;
seed_restarts_id = 1:10;
Ns = c(403);
p = 2
k = 2

res = list()
liks = list()
for (Ai in 1:length(A_types)) {
    res[[Ai]] = list()
    liks[[Ai]] = list()
    for (Ci in 1:length(C_types)) {
    res[[Ai]][[Ci]] = list()
    liks[[Ai]][[Ci]] = list()
        for (mi in 1:length(m_types)) {
            res[[Ai]][[Ci]][[mi]] = list()
            liks[[Ai]][[Ci]][[mi]] = list()
            for (Ni in 1:length(Ns)) {
                res[[Ai]][[Ci]][[mi]][[Ni]] = list()
                liks[[Ai]][[Ci]][[mi]][[Ni]] = list()
                for (si in 1:length(sampling_types)) {
                    res[[Ai]][[Ci]][[mi]][[Ni]][[si]] = list()
                    liks[[Ai]][[Ci]][[mi]][[Ni]][[si]] = list()
                    for (ei in 1:length(eig_types)) {
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]] = list()
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$As =
                        array(0,c(p,p,length(seed_simulation_id)))
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$Cs =
                        array(0,c(p,p,length(seed_simulation_id)))
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$Cs_st =
                        array(0,c(p,p,length(seed_simulation_id)))
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$mus =
                        array(0,c(p,k,length(seed_simulation_id)))
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$sigma2 =
                        array(0,c(p,k,length(seed_simulation_id)))
                        liks[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]] = list()
                         
                        for (sim_i in 1:length(seed_simulation_id)) {
                            liks[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]][[sim_i]] = 
                            rep(0,length(seed_restarts_id))
                            temp_res = list()
                            for (rs_i in 1:length(seed_restarts_id)) {
                                #print(c(Ai,Ci,mi,Ni,si,sim_i,rs_i))
                                outfile =
                                paste("results_eigen/",A_types[Ai],C_types[Ci],m_types[mi],Ns[Ni],sampling_types[si],eig_types[ei],sep="_")
                                outfile = paste(outfile,seed_restarts_id[rs_i],seed_simulation_id[sim_i],"EM_rs_res.Rdata",sep="_")
                                poss_err = tryCatch(load(outfile),error = function(e) e)
                            if (!inherits(poss_err,"error")) {
                            temp_res[[rs_i]] = outdat
                            liks[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]][[sim_i]][rs_i] = outdat$lpfinal
                            } else {
                            #print(Ai)
                            liks[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]][[sim_i]][rs_i] = -Inf;
                            print(outfile)
                            }
                            #print("loaded")
                        }
                        print(c(Ai,Ci,mi,Ni,si,ei,sim_i))
                        print(sort(liks[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]][[sim_i]]))
                        indmax =
                        which.max(liks[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]][[sim_i]][2:length(seed_restarts_id)]) + 1
                        #print(indmax)
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$As[,,sim_i] = temp_res[[indmax]]$A
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$Cs[,,sim_i] = temp_res[[indmax]]$C
                        print(temp_res[[indmax]]$C)
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$Cs_st[,,sim_i] = standardize_C(temp_res[[indmax]]$C,Ctrue[,,Ci])
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$mus[,,sim_i] = temp_res[[indmax]]$mu
                        res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$sigma2[,,sim_i] = temp_res[[indmax]]$sigma2
                    }
                #pdf(paste("A",Ai,Ci,mi,Ns[Ni],si,'.pdf',sep="_"))
                #plot_hist_array(res[[Ai]][[Ci]][[mi]][[Ni]][[si]]$As,Ai,Ci,mi,Ns[Ni],si,"A")
                #dev.off()
                #pdf(paste("C",Ai,Ci,mi,Ns[Ni],si,'.pdf',sep="_"))
                #plot_hist_array(res[[Ai]][[Ci]][[mi]][[Ni]][[si]]$Cs_st,Ai,Ci,mi,Ns[Ni],si,"C")
                #dev.off()
                pdf(paste("/Users/alextank/Documents/subsamp_svar/SubSamp_KDD/A_C",Ai,Ci,mi,Ns[Ni],si,eig_types[ei]*10,'.pdf',sep="_"),height = 5,width=10)          
                plot_hist_array_A_C(res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$As,res[[Ai]][[Ci]][[mi]][[Ni]][[si]][[ei]]$Cs_st,Ai,Ci,mi,Ns[Ni],sampling_types[si]-1,Atrue[,,Ai]*eig_types[ei],Ctrue[,,Ci],eig_types[ei])
                dev.off()
                
                }
            }
            plot_eigen_error(res[[Ai]][[Ci]][[mi]][[Ni]],Ai,Ci,mi,Ns[Ni],sampling_types - 1,Atrue[,,Ai],Ctrue[,,Ci],eig_types)
        }
    }
}
}

