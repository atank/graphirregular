#setwd("/Users/alextank/Documents/GraphIrregular/code/Rcode")
setwd("/Users/alextank/graphirregular/code/Rcode")
#setwd("/home/alextank/Documents/testgraph/graphirregular/code/Rcode")
library("Rcpp")
library("RcppArmadillo")
source("sampler.R")
source("interpolate.R")
source("visualize_results.R")
source("VAR_subsamp.R")
sourceCpp("sampler_cpp.cpp")
source("simulation_helper_funcs.R")
library("foreach")
library("doParallel")
iters = 100
cl<-makeCluster(8)
registerDoParallel(cl)
#ls<-foreach(icount(iters)) %dopar% {
  
#  to.ls<-rnorm(1e6)
#  to.ls<-summary(to.ls)
#  to.ls
  
#}
#iter = 
#foreach(a = icount(iters),.combine='cbind') %:% 
#  foreach(b = icount(iters),.combine='c') %do% {
#    a + b
#  } 

  seedb = 2378
  k = 1
  N = 500
  sd_errors = 1
  spikenslab = 0
  sampling_type = 2
  A_type = 2
  p = 2
  alpha = 100
  mu_sig = .1
  shape = .9
  rate  = .01
  ptm <- proc.time()
  
iters = 20
ls <- foreach(i = icount(iters),.combine = "cbind") %:%
  foreach(k = icount(2),.combine='c') %do% {
    #for (i in 1:iters) {
     # for (k in 1:2) {
    #library("Rcpp")
    #library("RcppArmadillo")
    #source("sampler.R")
    #source("interpolate.R")
    #source("visualize_results.R")
    #source("VAR_subsamp.R")
    #sourceCpp("sampler_cpp.cpp")
    #source("simulation_helper_funcs.R")
    
  seed = i*seedb
    prefix = "subsampVar"
    outfile = paste(prefix,"_",seed,"_",k,"_",N,"_",sd_errors,"_",spikenslab,"_",sampling_type,"_",A_type,"_",p,".csv",sep="")
    
    
    #########begin simulation#########
    set.seed(seed)
    sampling = sampling_select(sampling_type,p)
    
    A = A_select(A_type,p)
    p = length(sampling)
    simulation = list()
    
    simulation$err_type = 'Gaussian_Mixture'
    simulation$k = k;
    simulation$sigma = .1;
    simulation$lag = 1;
    simulation$sd = .25;
    simulation$p = p;
    simulation$alpha = alpha;
    simulation$mu_sig = mu_sig;
    simulation$shape = shape;
    simulation$rate = rate;
    
    output = VAR_subsamp_scale(N,sampling,A,simulation,sd_errors);
    output = VAR_subsamp_scale_fixed_errs(N,sampling,A,simulation,sd_errors);
    
    X = output$x
    z = output$z
    mean_err = output$mean_err
    var_err = output$var_err
    xtrue = output$xtrue
    simulation$mu = output$mum
    simulation$sig = output$sig
    simulation$theta = output$theta
    
    ####start sampler#####
    param = list()
    param$A = A
    param$A = matrix(rnorm(p*p,0,1)^2,p,p)
    
    param$p = p
    param$lag = 1
    param$xinit = interpolate_block(X,p,N)
    #param$xinit = xtrue;
    param$alpha = 1;
    param$zinit = z;
    param$sigma_A = .8;
    param$lambda = 1;
    param$alpha_gamma = 1;
    param$beta_gamma = 1;
    param$P0 = diag(200,p);
    param$spikenslab=spikenslab;
    param$gamma = matrix(1,p,p);
    param$phi = .1;
    
    param$k = simulation$k
    param$thetam = output$thetam
    param$mum = output$mum
    param$sigmam = output$sigmam
    
    param$mum = matrix(rnorm(param$p*param$k,0,1),param$p,param$k)
    param$sigmam = matrix(rnorm(param$p*param$k,0,1)^2,param$p,param$k)
    param$thetam = rdirichlet(param$p,rep(1,param$k)*10)
    
    if (param$k == 1) {
      thetam = rep(1,p);
      mum = rep(0,p);
    } 
    
    nsamp = 4000
    nburn = 1000
    output = mixedfreq_sampler(nsamp,nburn,param,N,p)
    
    
    ######get means of error outputs####
    mu_est = apply(output[[1]],c(1,2),mean)
    sig_est = apply(output[[2]],c(1,2),mean)
    theta_est = apply(output[[4]],c(1,2),mean)
    msamp = apply(output[[6]],c(1,2),mean)
    qq=2
    
    outpute = total_variation(simulation$mu,simulation$sig,simulation$theta,output[[1]],output[[2]],output[[4]],param$p,param$k,nsamp)
    totalvar = outpute[[1]]
    dentrue = outpute[[2]]
    denest = outpute[[3]]
    
    
    gammatrue = matrix(0,p,p)
    gammatrue[which(A == 0)] = 0
    gammatrue[which(A != 0)] = 1
    
    Aest= apply(output[[5]],c(1,2),mean)
    print(Aest)
    gammaest = apply(output[[7]],c(1,2),mean)
    
    errA = norm(Aest - A,"F")/norm(A,"F")
    output=compute_roc(A,Aest)
    aucA=output[[2]]
    
    errGam = norm(gammaest - gammatrue,"F")/norm(gammatrue)
    output=compute_roc(gammatrue,gammaest)
    aucGamma=output[[2]]
    
    #write.csv(c(errA,aucA,errGam,aucGamma,totalvar), file=paste("results/","sum",outfile,sep=""),col.names=F, row.names=F)
    #write.table(A,file=paste("results/","As",outfile,sep=""),col.names=F,row.names=F)
    #write.table(Aest,file=paste("results/","Aest",outfile,sep=""),col.names=F,row.names=F)
    #write.table(gammatrue,file=paste("results/","Gs",outfile,sep=""),col.names=F,row.names=F)
    #write.table(gammaest,file=paste("results/","Gest",outfile,sep=""),col.names=F,row.names=F)
    #write.table(denest,file=paste("results/","denest",outfile,sep=""),col.names=F,row.names=F)
    #write.table(dentrue,file=paste("results/","dentrue",outfile,sep=""),col.names=F,row.names=F)
    
    errA
    
      }
    #}
set = proc.time() - ptm

