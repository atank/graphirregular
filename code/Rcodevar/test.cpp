#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

using namespace arma;
using namespace Rcpp;

//  [[Rcpp::export]]
NumericVector ssmooth(int nk) {
arma::cube D = zeros<cube>(nk, nk, nk);
arma::cube Q(nk,nk,nk);
NumericVector q = as<NumericVector>(wrap(D));
return q;
}
