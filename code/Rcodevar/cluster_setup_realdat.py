from itertools import product
import os

m_type = [1,3,4,5]
sample_C = [0,1]
realdata = ["pair0050.txt"]
#["pair0049.txt","pair0050.txt","pair0051.txt"]
#,"pair0065.txt","pair0066.txt","pair0067.txt"]
#realdata = ["pair0068.txt","pair0096.txt","pair0072.txt","pair0087.txt","pair0099.txt"]

basejfile = "EM_slurm.sbatch"
par_prod = product(m_type,realdata,sample_C)
for par in par_prod:
    mt,rd,sC  = par
    rd_nop = rd.replace(".", "")
    jfile_b = "rd-" + rd_nop + "-mt-%d" % (mt) + "-sC-%d" % (sC)
    jfile = "sbatch_files/" + jfile_b + basejfile
    with open(jfile,'w') as f:
        f.write("#!/bin/bash" + "\n")
        f.write("\n")
        f.write("#BATCH --job-name tankjob" + "\n")
        f.write("#SBATCH --partition medium" + "\n")
        f.write("#SBATCH --ntasks 1" + "\n")
        f.write("#SBATCH --time 2-00:00" + "\n")
        f.write("#SBATCH --mem-per-cpu=4000M" + "\n")
        f.write("#SBATCH -o outs/" + jfile_b + "EM_sim_%j.out" +"\n")
        f.write("#SBATCH -e outs/" + jfile_b + "EM_sim_%j.err" + "\n")
        f.write("#SBATCH --mail-user=alextank@uw.edu" + "\n")
        f.write("\n")
        args = "'--args " + "dat_file=\"" +  rd  + "\"  sampling_type=%d " %(mt) + "sample_C=%d '" %(sC)
        temp = rd_nop + "-%d" % (mt) + "-%d" % (sC)
        f.write("R CMD BATCH " + args + " realdat_slurm_EM.R " + "outs/" + temp + '_' + '"${SLURM_ARRAY_TASK_ID}".Rout')
    os.chmod(jfile,0755)

jfile_r = "master_EM_subsamp_slurm_realdat.sh"
par_prod = product(m_type,realdata,sample_C)
with open(jfile_r,'w') as f:
    f.write("#!/bin/bash" + "\n")
    f.write("\n")
    for par in par_prod:
        mt,rd,sC = par
        rd_np = rd.replace(".","")
        jfile_b = "sbatch_files/" + "rd-" + rd_np +"-mt-%d" % (mt) + "-sC-%d" % (sC)
        jfile = jfile_b + basejfile
        f.write("sbatch --array=1-600 " + jfile + "\n")
os.chmod(jfile_r,0755)


