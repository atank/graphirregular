from itertools import product
import os

As = [1]
Cs = [2]
m_type = [3]
#sampling_t = [3,4]
sampling_t = [2,6]
Nt = [403]
eig = [1,.9,.8,.7,.6,.5]
exp_type = [2]


basejfile = "EM_slurm_eig.sbatch"
par_prod = product(As,Cs,m_type,sampling_t,Nt,eig,exp_type)
for par in par_prod:
    A,C,mt,st,nt,ei,ety = par
    jfile_b = "exp-A-%d-C-%d-mt-%d-st-%d-Nt-%d-eig-%.1f-expt-%d" % (A,C,mt,st,nt,ei,ety)
    jfile = "sbatch_files/" + jfile_b + basejfile
    with open(jfile,'w') as f:
        f.write("#!/bin/bash" + "\n")
        f.write("\n")
        f.write("#BATCH --job-name tankjob" + "\n")
        f.write("#SBATCH --partition short" + "\n")
        f.write("#SBATCH --ntasks 1" + "\n")
        f.write("#SBATCH --time 0-08:00" + "\n")
        f.write("#SBATCH --mem-per-cpu=4000M" + "\n")
        f.write("#SBATCH -o outs/" + jfile_b + "EM_sim_%j.out" +"\n")
        f.write("#SBATCH -e outs/" + jfile_b + "EM_sim_%j.err" + "\n")
        f.write("#SBATCH --mail-user=alextank@uw.edu" + "\n")
        f.write("\n")
        args = "'--args A_type=%d C_type=%d m_type=%d sampling_type=%d N=%d eig_s=%.1f exp_type=%d'" % (A,C,mt,st,nt,ei,ety)
        temp = "%d_%d_%d_%d_%d_%.1f_%d" % (A,C,mt,st,nt,ei,ety)
        f.write("R CMD BATCH " + args + " sim_slurm_EM.R " + "outs/" + temp + '_' + '"${SLURM_ARRAY_TASK_ID}".Rout')
    os.chmod(jfile,0755)

jfile_r = "master_EM_subsamp_slurm_eig.sh"
par_prod = product(As,Cs,m_type,sampling_t,Nt,eig,exp_type)
with open(jfile_r,'w') as f:
    f.write("#!/bin/bash" + "\n")
    f.write("\n")
    for par in par_prod:
        A,C,mt,st,nt,ei,ety = par
        jfile_b = "sbatch_files/exp-A-%d-C-%.d-mt-%d-st-%d-Nt-%d-eig-%.1f-expt-%d" % (A,C,mt,st,nt,ei,ety)
        jfile = jfile_b + basejfile
        f.write("sbatch --array=1-400 " + jfile + "\n")
os.chmod(jfile_r,0755)


