#include <RcppArmadillo.h>
#include <Rcpp.h>
using namespace Rcpp;
using namespace arma;

// [[Rcpp::depends(RcppArmadillo)]]

void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}
void showValue_int(int x) {
    Rcout << "The value is " << x << std::endl;
}
void showMatrix(arma::mat X) {
    Rcout << "Armadillo matrix is" << std::endl << X << std::endl;
}

void showVec(arma::vec X) {
    Rcout << "Armadillo matrix is" << std::endl << X << std::endl;
}

void showVector(NumericVector v) {
      Rcout << "NumericVector is" << std::endl << v << std::endl;

}
double logsumexp(NumericVector x) {
  return(log(sum(exp(x - max(x)))) + max(x));
}

//[[Rcpp::export]]
arma::mat M_C_cpp(List param,List hyperparam, List E) {

  List Exz_tm1_tm1 = E["Exz_tm1_tm1"];
  List Exz_tm1 = E["Exz_tm1"];
  List Exz_t_tm1 = E["Exz_t_tm1"];
  List Exz_t_t = E["Exz_t_t"];
  arma::cube Ez = as<arma::cube>(E["Ez"]);
  List Exz_t = E["Exz_t"];
  arma::mat A = param["A"];
  arma::mat W = param["W"];
  NumericMatrix sigma2 = param["sigma2"];
  NumericMatrix mu = param["mu"];
  arma::mat C = inv(C);
  arma::mat x = param["x"];
  uvec nonzeroW = as<uvec>(hyperparam["nonzeroW"]) - 1;
  int p = hyperparam["p"];
  int k = hyperparam["k"];
  int N = hyperparam["N"];
  double thresh = .00001;
  int diff = 100;
  arma::mat d(p,p);
  d.zeros();
  arma::mat grad(p,p);
  grad.zeros();
  
  arma::mat perm_mat(p*p,p*p);perm_mat.zeros();
  for (int i = 1; i <= p*p; i++) {
    int temp = (i - 1) % p;
    int temp2 = (int)floor(((double)(i - 1))/((double)p));
    perm_mat(i - 1,temp*p + temp2) = 1;
  }
  arma::mat hess(p*p,p*p);
  
  
  while(diff > thresh) {
    grad.zeros();
    hess.zeros();
    C = inv(W);
    hess = -(N-1)*(perm_mat*kron(C.t(),C));
    grad = (N - 1)*C.t();
    
    for (int i = 0; i < p;i++) {
      List Exz_tm1_tm1i = Exz_tm1_tm1[i]; List Exz_tm1i = Exz_tm1[i]; List Exz_t_tm1i = Exz_t_tm1[i];
      List Exz_ti = Exz_t[i]; List Exz_t_ti = Exz_t_t[i];
      for (int j = 0; j < k; j++) {
        arma::cube Exz_tm1_tm1ij = Exz_tm1_tm1i[j]; arma::mat Exz_tm1ij = Exz_tm1i[j]; arma::cube Exz_t_tm1ij = Exz_t_tm1i[j];
        arma::mat Exz_tij = Exz_ti[j];arma::cube Exz_t_tij = Exz_t_ti[j];
        for (int t = 1; t < N; t++) {
          grad.row(i) += (- Exz_t_tij.slice(t) * W.row(i).t() - 
                                   A * Exz_tm1_tm1ij.slice(t) * A.t() * W.row(i).t() +
                                   (Exz_t_tm1ij.slice(t) * A.t() + (Exz_t_tm1ij.slice(t) * A.t()).t()) * W.row(i).t() +
                                   Exz_tij.row(t).t()*mu(i,j) - 
                                   A * Exz_tm1ij.row(t).t()*mu(i,j)).t()/sigma2(i,j);
          arma::mat hess_p = (- Exz_t_tij.slice(t) - A * Exz_tm1_tm1ij.slice(t) * A.t() +
           (Exz_t_tm1ij.slice(t) * A.t() + (Exz_t_tm1ij.slice(t) * A.t()).t()))/sigma2(i,j);
          d(i,i) = 1;
          hess += kron(hess_p,d);
          d(i,i) = 0;
        }
      }
    }
    arma::mat W_old = W;
    arma::vec grad_vec = vectorise(grad);
    
    arma::vec grad_vec2 = grad_vec.elem(nonzeroW);
    arma::mat hess2 = hess.submat(nonzeroW,nonzeroW);
    vec Wvec2 = inv(hess2) * grad_vec2;
    vec Wvec = arma::vec(p*p);
    Wvec.fill(0);
    Wvec.elem(nonzeroW) = Wvec2;  
    arma::mat W_add;
    W_add.insert_cols(0,Wvec);
    W_add.reshape(p,p);
    W -= W_add;
    
    diff = norm(W - W_old,"fro");
  }
  return(W);
}
    
    
    
    


//[[Rcpp::export]]
NumericMatrix M_sig2_cpp(List param, List hyperparam, List E) {
  arma::mat W = param["W"];
  int N = hyperparam["N"];
  int p = hyperparam["p"];
  int k = hyperparam["k"];
  NumericMatrix mu = param["mu"];
  arma::mat B(p,p);
  List Exz_tm1_tm1 = E["Exz_tm1_tm1"];
  List Exz_tm1 = E["Exz_tm1"];
  List Exz_t_tm1 = E["Exz_t_tm1"];
  bool opt_W = hyperparam["opt_W"];
  int start_ind = 0;
  arma::mat A = param["A"];
  List Exz_t_t = E["Exz_t_t"];
  arma::cube Ez = as<arma::cube>(E["Ez"]);
  List Exz_t = E["Exz_t"];
  NumericMatrix sigma2(p,k);
  double sig_thresh = .000001;
  if (opt_W) {
    start_ind = 1;
    sigma2(0,0) = 1;sigma2(1,0) = 1; 
  }
  for (int i = 0; i < p; i++) {
    List Exz_tm1_tm1i = Exz_tm1_tm1[i]; List Exz_tm1i = Exz_tm1[i]; List Exz_t_tm1i = Exz_t_tm1[i];
    List Exz_ti = Exz_t[i]; List Exz_t_ti = Exz_t_t[i];
    for (int j = start_ind; j < k; j++) {

      arma::cube Exz_tm1_tm1ij = Exz_tm1_tm1i[j]; arma::mat Exz_tm1ij = Exz_tm1i[j]; arma::cube Exz_t_tm1ij = Exz_t_tm1i[j];
      arma::mat Exz_tij = Exz_ti[j];arma::cube Exz_t_tij = Exz_t_ti[j];
      double sumij = 0;
      for (int t = 1; t < N; t++) {
        sigma2(i,j) += as<double>(wrap(W.row(i) * Exz_t_tij.slice(t) * W.row(i).t() +
        W.row(i) * A * Exz_tm1_tm1ij.slice(t) * A.t() * W.row(i).t()
        + mu(i,j)*mu(i,j)*Ez(i,j,t) - 
        2*mu(i,j)*W.row(i) * Exz_tij.row(t).t() -
        2*W.row(i)*Exz_t_tm1ij.slice(t)*A.t() * W.row(i).t() + 
        2*mu(i,j)*W.row(i)*A * Exz_tm1ij.row(t).t()));
        sumij += Ez(i,j,t); 
      }
      sigma2(i,j) /= sumij;
      if (sigma2(i,j) < sig_thresh) {
        sigma2(i,j) = sig_thresh;
      }
    }
  }
  return(sigma2);
}


//[[Rcpp::export]]
arma::mat M_A_cpp(List param,List hyperparam, List E) {
  arma::mat W = param["W"];
  int N = hyperparam["N"];
  int p = hyperparam["p"];
  int k = hyperparam["k"];
  NumericMatrix sigma2 = param["sigma2"];
  NumericMatrix mu = param["mu"];
  arma::mat B(p,p);
  List Exz_tm1_tm1 = E["Exz_tm1_tm1"];
  List Exz_tm1 = E["Exz_tm1"];
  List Exz_t_tm1 = E["Exz_t_tm1"];
  for (int i = 0; i < p;i++) {
    arma::mat temp_mat(p,p);
    temp_mat.zeros();
    arma::colvec temp_bs(p);
    temp_bs.zeros();
    List Exz_tm1_tm1i = Exz_tm1_tm1[i];
    List Exz_tm1i = Exz_tm1[i];
    List Exz_t_tm1i = Exz_t_tm1[i];
    for (int j = 0; j < k; j++) {
      arma::cube Exz_tm1_tm1ij = Exz_tm1_tm1i[j];
      arma::mat Exz_tm1ij = Exz_tm1i[j];
      arma::cube Exz_t_tm1ij = Exz_t_tm1i[j];
      for (int t = 1; t < N;t ++) {
        temp_mat += Exz_tm1_tm1ij.slice(t)/sigma2(i,j);
        temp_bs += (-mu(i,j)*Exz_tm1ij.row(t).t() + Exz_t_tm1ij.slice(t).t() * W.row(i).t())/sigma2(i,j);
      }
    }
    B.row(i) = (inv(temp_mat) * temp_bs).t();
  }
  arma::mat A = inv(W) * B;
  return(A);
}

double lmvdnorm_cpp(arma::colvec x,arma::colvec mu,arma::mat Cov) {
  double val;
  double sign;
  log_det(val,sign,Cov);
  double l = -.5*val;
  l -= as<double>(wrap(.5*(x - mu).t() * inv(Cov) * (x - mu)));
  l -= (x.n_elem/2.0)*log(2*M_PI);
  return(l);
}




//[[Rcpp::export]]
List forward_backward_smoothing_cpp(IntegerMatrix Z,List param,List hyperparam,int base_time,List Y) {
  arma::mat A = param["A"];
  arma::mat sigma2 = param["sigma2"];
  arma::mat mu = param["mu"];
  arma::mat W = param["W"];
  arma::mat C = param["C"];
  List B = hyperparam["B"];
  double loglik = 0;
  int p = hyperparam["p"];
  int sub = hyperparam["sub"];
  arma::mat mut(sub+1,p);mut.zeros();
  mut.row(0) = as<arma::rowvec>(Y[base_time-1]);
  arma::mat mut_tm1(sub+1,p);mut_tm1.zeros();
  arma::cube Sigmat(p,p,sub+1);Sigmat.zeros();
  Sigmat.slice(0).zeros();
  arma::cube Sigmat_tm1(p,p,sub+1);Sigmat_tm1.zeros();
  arma::mat D(p,p);
  D.zeros();
  arma::colvec u(p);
  u.zeros();
  arma::cube Ex_t_t(p,p,sub+1); Ex_t_t.zeros();
  arma::cube Ex_t_tm1(p,p,sub+1); Ex_t_tm1.zeros();
  arma::mat mut_T(sub+1,p); mut_T.zeros();
  mut_T.row(0) = mut.row(0);
  arma::cube Sigmat_T(p,p,sub+1);Sigmat_T.zeros();

  //filtering step
  for (int t = 1; t < sub + 1; t++) {
    arma::colvec yt = Y[base_time + t - 1];
    for (int i = 0; i < p; i++) {
      u(i) = mu(i,Z(t-1,i)-1);
      D(i,i) = sigma2(i,Z(t-1,i)-1);
    }
    mut_tm1.row(t) = (A * mut.row(t-1).t() + C * u).t();
    Sigmat_tm1.slice(t) = A * Sigmat.slice(t-1)*A.t() + C * D * C.t();
    arma::mat St;
    arma::mat Bt;
    if (yt.n_elem == 1) {
      arma::rowvec st = as<arma::rowvec>(B[t]);
      Bt = conv_to<arma::mat>::from(st);
    }
    else {
      Bt = as<arma::mat>(B[t]);
    }
    St = Bt * Sigmat_tm1.slice(t) * Bt.t();

    if (yt.n_elem > 0) {
      loglik += lmvdnorm_cpp(yt,Bt*mut_tm1.row(t).t(),St);
    }
    if (yt.n_elem < p) {
      if (yt.n_elem == 0) {
        mut.row(t) = mut_tm1.row(t);
        Sigmat.slice(t) = Sigmat_tm1.slice(t);
      }
      else {      
        arma::vec ytilde = yt - Bt*mut_tm1.row(t).t();
        arma::mat Kt = Sigmat_tm1.slice(t)*Bt.t()*inv(St);
        mut.row(t) = mut_tm1.row(t) + (Kt*ytilde).t();
        Sigmat.slice(t) = Sigmat_tm1.slice(t) - Kt*Bt*Sigmat_tm1.slice(t);
      }
    } 
    if (yt.n_elem == p) {
      mut.row(t) = yt.t();
      Sigmat.slice(t).zeros();
      mut_T.row(t) = mut.row(t);
      Sigmat_T.slice(t) = Sigmat.slice(t);
      Ex_t_t.slice(t) = mut_T.row(t).t() * mut_T.row(t);
    }
  }

  ///smoothing step
  if (sub > 1) {
    for (int t = (sub - 1); t >= 1; t--) {
      arma::mat Jt = Sigmat.slice(t) * A.t() * inv(Sigmat_tm1.slice(t+1));
      Sigmat_T.slice(t) = Sigmat.slice(t) + Jt * (Sigmat_T.slice(t+1) - Sigmat_tm1.slice(t+1)) * Jt.t();
      mut_T.row(t) = mut.row(t) + (Jt * (mut_T.row(t+1).t() - mut_tm1.row(t+1).t())).t();
      Ex_t_tm1.slice(t+1) = Sigmat_T.slice(t+1) * Jt.t() + mut_T.row(t+1).t() * mut_T.row(t);
      Ex_t_t.slice(t) = Sigmat_T.slice(t) + mut_T.row(t).t() * mut_T.row(t);
    }
  }
  Ex_t_tm1.slice(1) = mut_T.row(1).t() * mut_T.row(0);
  Ex_t_t.slice(0) = mut_T.row(0).t() * mut_T.row(0);
  return Rcpp::List::create(Ex_t_tm1, Ex_t_t,mut_T,loglik);
}


double log_probability_zs_cpp(IntegerMatrix z_temp,NumericMatrix pi,int p) {
  double logp = 0;
  for (int i = 0; i < p; i++) {
    for (int j = 0;j < z_temp.nrow();j++) {
      logp += log(pi(i,z_temp(j,i)-1));
    }
  }
  return(logp);
}
//[[Rcpp::export]]
List e_step_cpp(List param, List hyperparam, List Y,List z_temp_list) {

  int Nblocks = hyperparam["Nblocks"];
  IntegerMatrix zcombs = hyperparam["zcombs"];

  int p = hyperparam["p"];
  int k = hyperparam["k"];
  int sub = hyperparam["sub"];

  int Ncomb = zcombs.nrow();
  int N = hyperparam["N"];
  List Exz_t_t(p); List Exz_t(p); arma::cube Ez(p,k,N); Ez.zeros();
  List Exz_tm1_tm1(p);List Exz_t_tm1(p);List Exz_tm1(p); 
  List Ex_t_t(Ncomb); List Ex_t_tm1(Ncomb);List mut(Ncomb);
  arma::cube TExz_t_t(p,p,N*p*k); TExz_t_t.zeros();
  arma::cube TExz_tm1_tm1(p,p,N*p*k);TExz_tm1_tm1.zeros();
  arma::cube TExz_t_tm1(p,p,N*p*k); TExz_t_tm1.zeros();
  arma::mat TExz_t(N*p*k,p); TExz_t.zeros();
  arma::mat TExz_tm1(N*p*k,p);TExz_tm1.zeros();
  arma::mat mu_Avg(N,p); mu_Avg.zeros();
  arma::mat W = param["W"]; arma::mat C = inv(W); param["C"] = C;
  double log_lik_complete = 0;
  for (int i = 0; i < Nblocks; i++) {
    NumericVector logliks(Ncomb);
    int tstart = i*sub + 1;
    //int tend = i*sub + 1;
    for (int j = 0; j < Ncomb; j++) {

      IntegerMatrix z_temp = z_temp_list[j];
      List Es = forward_backward_smoothing_cpp(z_temp,param,hyperparam,tstart,Y);
      logliks(j) = Es[3];
      logliks(j) += log_probability_zs_cpp(z_temp,as<NumericMatrix>(param["pi"]),p);
      Ex_t_t[j] = Es[1];Ex_t_tm1[j] = Es[0]; mut[j] = Es[2];
    }

    double temp = logsumexp(logliks);
    log_lik_complete += temp;
    NumericVector probs = exp(logliks - temp);

    for (int j = 0; j < Ncomb; j++) {
      
      arma::cube Ex_t_tj = Ex_t_t[j]; arma::cube Ex_t_tm1j = Ex_t_tm1[j]; 
      arma::mat mutj = mut[j];
      IntegerMatrix z_temp = z_temp_list[j];
      for (int t = 1; t < (sub + 1); t++) {
        for (int l = 0; l < p; l++) {
          int ind = z_temp(t-1,l) - 1;
          int ti = tstart + t - 1;
          int ind_temp_temp = ind*p + l;
          int ind_temp = ind_temp_temp*N + ti;
          TExz_t_t.slice(ind_temp) += probs(j)*Ex_t_tj.slice(t);
          TExz_tm1_tm1.slice(ind_temp) += probs(j)*Ex_t_tj.slice(t-1);
          TExz_t_tm1.slice(ind_temp) += probs(j)*Ex_t_tm1j.slice(t);
          TExz_t.row(ind_temp) += probs(j)*mutj.row(t);
          TExz_tm1.row(ind_temp) += probs(j)*mutj.row(t-1);
          Ez(l,ind,ti) += probs(j);
        }
      }
    }
  }
  ///convert to data structure useful for output
  for (int l = 0; l < p; l++) {
    List Exz_t_tl(k);
    List Exz_tl(k);
    List Exz_tm1_tm1l(k); List Exz_t_tm1l(k);List Exz_tm1l(k);
    for (int j = 0; j < k; j++) {
      int temp_ind = j*p + l;
      int tstart = N*temp_ind;
      int tend = N*temp_ind + N - 1;
      arma::cube temp = TExz_t_t.slices(tstart,tend); Exz_t_tl[j] = temp;
      arma::cube temp2 = TExz_tm1_tm1.slices(tstart,tend); Exz_tm1_tm1l[j] = temp2;
      arma::cube temp3 = TExz_t_tm1.slices(tstart,tend); Exz_t_tm1l[j] = temp3;
      arma::mat temp4 = TExz_t.rows(tstart,tend); Exz_tl[j] = temp4;
      arma::mat temp5 = TExz_tm1.rows(tstart,tend); Exz_tm1l[j] = temp5;
    }
    Exz_t_t[l] = Exz_t_tl;Exz_t[l] = Exz_tl;Exz_tm1_tm1[l] = Exz_tm1_tm1l;
    Exz_t_tm1[l] = Exz_t_tm1l;Exz_tm1[l]=Exz_tm1l;
  }
  
  List output;
  output["Exz_t_t"] = Exz_t_t; output["Exz_tm1_tm1"] = Exz_tm1_tm1;
  output["Exz_t_tm1"] = Exz_t_tm1; output["Exz_t"] = Exz_t; output["Exz_tm1"] = Exz_tm1;
  output["Ez"] = Ez;output["full_loglik"] = log_lik_complete;
  return(output);
}

