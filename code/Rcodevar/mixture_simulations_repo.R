#####potential mixture distributions to use in simulations
source("simulation_helper_funcs.R")
sigma = c(.2, 1)
mu = c(-1,.2)
pi = c(.7,.3)
mu = mu - sum(pi*mu)
mom = moments_mixture_normal(sigma,mu,pi)
plot(mom[[5]],mom[[4]],type = 'l')
mom[[2]]
mom[[3]]

sigma = c(.2, 1)
mu = c(1,-.2)
pi = c(.7,.3)
mu = mu - sum(pi*mu)
mom = moments_mixture_normal(sigma,mu,pi)
plot(mom[[5]],mom[[4]],type = 'l')
mom[[2]]
mom[[3]]
