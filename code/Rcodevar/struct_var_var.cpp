#include <RcppArmadillo.h>
#include <Rcpp.h>
using namespace Rcpp;
using namespace arma;

// [[Rcpp::depends(RcppArmadillo)]]

void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}
void showValue_int(int x) {
    Rcout << "The value is " << x << std::endl;
}
void showMatrix(arma::mat X) {
    Rcout << "Armadillo matrix is" << std::endl << X << std::endl;
}
double logsumexp(NumericVector x) {
  return(log(sum(exp(x - max(x)))) + max(x));
}


arma::cube optimize_theta(arma::cube x_cross,arma::cube x_cross1,arma::mat x,arma::mat mu,arma::mat sigma,arma::mat B,arma::mat Cinv,arma::mat pi,int p,int k, int N,double beta) {
  arma::cube theta(N+1,p,k);
  NumericVector theta_temp(k);
  for (int i = 1; i < N+1;i++) {
    for (int m = 0; m < p; m++) {
      arma::vec base = - Cinv.row(m) * x_cross.slice(i) * Cinv.row(m).t();
      base += 2.0 * B.row(m) * x_cross1.slice(i) * Cinv.row(m).t();
      base += - B.row(m)*x_cross.slice(i-1) * B.row(m).t();
      //showValue(as<double>(wrap(base)));
      for (int j = 0; j < k;j++) {
        double temp = - as<double>(wrap(2.0*B.row(m)*x.col(i-1)*mu(m,j)));
        temp += as<double>(wrap(2.0*Cinv.row(m)*x.col(i)*mu(m,j)));
        temp += as<double>(wrap(- mu(m,j)*mu(m,j) + base));
        //showValue(temp);
        //showValue(1);
        theta_temp(j) = temp/(2.0*sigma(m,j));
        theta_temp(j) += log(pi(m,j)) - .5*log(sigma(m,j));
        theta_temp(j) *= beta;
        //showValue(theta_temp(j));
        //showValue(sigma(m,j));
      }
      double total = logsumexp(theta_temp);
      for (int j = 0;j < k;j++) {
        theta(i,m,j) = exp(theta_temp(j) - total);
      }
    }
  }
  return theta;
} 

arma::mat optimize_pi(arma::cube theta,int k,int  p,int N) {
  double pimin = .0000001;
  arma::mat pi = zeros<mat>(p,k);
  for (int i = 0;i < p;i++) {
    for (int j = 0;j < k;j++) {
      for (int n = 1;n <= N;n++) {
        pi(i,j) += theta(n,i,j)/N;
      }
    }
  }
  for (int i = 0;i < p;i++) {
    double sum = 0;
    for (int j = 0;j < k;j++) {
      if (pi(i,j) < pimin) {
        pi(i,j) = pimin;
      }
      sum += pi(i,j);
    }
    for (int j = 0;j < k;j++) {
      pi(i,j) = pi(i,j)/sum;
    }
  }
  return(pi);
}
//
arma::mat optimize_mu(arma::mat x,arma::cube theta,arma::mat Cinv,arma::mat B, int k, int p, int N) {
  arma::mat mu(p,k);
  for (int i = 0;i < p;i++) {
    for (int j = 0; j < k;j++) {
      double num = 0.0;
      double denom = 0.0;
      for (int n = 1;n <= N;n++) {
        //showValue(theta(n,i,j));
        num += theta(n,i,j)*as<double>(wrap((Cinv.row(i)*x.col(n) - B.row(i)*x.col(n-1))));
        denom += theta(n,i,j);
      }
      //showValue(denom);
      mu(i,j) = num/(denom);
    }
  }
  return mu;
}
//

arma::mat optimize_sigma2(arma::mat x,arma::cube x_cross0,arma::cube x_cross1,arma::cube theta,arma::mat B, arma::mat Cinv,arma::mat mu,int N, int p, int k) {
  arma::mat sigma = zeros<mat>(p,k);
  for (int i = 0;i < k;i++) {
    for (int j = 0;j < p;j++) {
      double total = 0.0;
      double temp = 0.0;
      for (int n = 1;n <= N;n++) {
        temp += as<double>(wrap(theta(n,j,i)))*as<double>(wrap((Cinv.row(j)*x.col(n) - B.row(j)*x.col(n-1) - mu(j,i))*(Cinv.row(j)*x.col(n) - B.row(j)*x.col(n-1) - mu(j,i))));
        total += theta(n,j,i);
      }
      //showValue(total);
      sigma(j,i) = as<double>(wrap(temp))/as<double>(wrap(total));
    }
  }
  return(sigma);
}

arma::mat optimize_sigma(arma::mat x,arma::cube x_cross0,arma::cube x_cross1,arma::cube theta,arma::mat B, arma::mat Cinv,arma::mat mu,int N, int p, int k) {
  arma::mat sigma(p,k);
  bool inf_C = false;
  double sigmin = .000001;
  int starti = 0;
  if (inf_C) {
    for (int j = 0; j < p;j++) {
      sigma(j,0) = 1;
    }
    starti = 1;
  }
  for (int i = starti; i < k;i ++) {
    for (int j = 0; j < p;j ++) {
      arma::mat sigma0 = zeros<mat>(p,p);
      arma::mat sigma0m1 = zeros<mat>(p,p);
      arma::mat sigma1 = zeros<mat>(p,p);
      arma::vec mum1av = zeros<vec>(p);
      arma::vec muav = zeros<vec>(p);
      double test = 0.0;
      double total = 0.0;
      for (int n = 1;n <= N; n++) {
        sigma0 += theta(n,j,i)*x_cross0.slice(n);
        sigma0m1 += theta(n,j,i)*x_cross0.slice(n-1);
        sigma1 += theta(n,j,i)*x_cross1.slice(n).t();
        mum1av += theta(n,j,i)*x.col(n-1);
        muav += theta(n,j,i)*x.col(n);
        total += theta(n,j,i);
        test += as<double>(wrap(theta(n,j,i)*(Cinv.row(j)*x.col(n) - B.row(j)*x.col(n-1) - mu(j,i))*(Cinv.row(j)*x.col(n) - B.row(j)*x.col(n-1) - mu(j,i)))); 
      }
      double temp = 0;
      temp = as<double>(wrap(Cinv.row(j) * sigma0 * Cinv.row(j).t() + B.row(j) * sigma0m1 * B.row(j).t())); 
      temp += as<double>(wrap(-2.0*Cinv.row(j)*sigma1*B.row(j).t()));
      temp += as<double>(wrap(-2.0*mu(j,i)*(-B.row(j)*mum1av + Cinv.row(j)*muav) + total*mu(j,i)*mu(j,i)));
      sigma(j,i) = temp/(total);
      //sigma(j,i) = as<double>(wrap(test))/as<double>(wrap(total));
      if (sigma(j,i) < sigmin) {
        sigma(j,i) = sigmin;
      }
      //showValue(temp/total - test/total);
    }
  }
  return sigma;
}
//
List calc_moments(arma::mat x,int N,int p,int k) {
  arma::cube x_cross0 = zeros<cube>(p,p,N+1);
  arma::cube x_cross1 = zeros<cube>(p,p,N+1);

  for (int i = 0; i <= N; i++) {
    x_cross0.slice(i) = x.col(i)*x.col(i).t();
    if (i > 0) {
      x_cross1.slice(i) = x.col(i-1)*x.col(i).t();
    }
  }
  NumericVector x_cross0n = as<NumericVector>(wrap(x_cross0));
  NumericVector x_cross1n = as<NumericVector>(wrap(x_cross1));  
  return Rcpp::List::create(x_cross0n, x_cross1n);
}
//

arma::mat optimize_B(arma::mat x,arma::cube x_cross0,arma::cube x_cross1,arma::mat mu,arma::mat sigma,int p,int k,int N,arma::mat Cinv,arma::cube theta) {
  arma::mat B(p,p);
  for (int i = 0; i < p;i++) {
    arma::mat xxt = zeros<mat>(p,p);
    arma::vec xy = zeros<vec>(p);
    for (int n = 1; n <= N;n++) {
      for (int j = 0; j < k;j++) {
        xxt += (theta(n,i,j)/sigma(i,j))*x_cross0.slice(n-1);
        arma::mat temp = Cinv.row(i)*x_cross1.slice(n).t();
        xy += (theta(n,i,j)/sigma(i,j))*(temp.t() - mu(i,j)*x.col(n-1));
      }
    }
    B.row(i) = solve(xxt,xy).t();
  }
  return B;
}

double calc_differences(arma::mat pi_old,arma::mat pi,arma::mat B_old,arma::mat B,arma::mat Cinv_old,arma::mat Cinv,arma::mat mu_old,arma::mat mu,arma::mat sigma_old,arma::mat sigma) {
  double pi_diff = norm(pi_old - pi,"fro");
  double B_diff = norm(B_old - B,"fro");
  double Cinv_diff = norm(Cinv_old - Cinv,"fro");
  double mu_diff = norm(mu_old - mu,"fro");
  double sigma_diff = norm(sigma_old - sigma,"fro");
  return max(NumericVector::create(pi_diff,B_diff,Cinv_diff,mu_diff,sigma_diff));
}

List forward_backward_cpp(arma::cube theta,arma::mat mu,arma::mat sigma,arma::mat B,arma::mat Cinv, List Xsqueezed,List param,arma::mat obs,double beta) {
  int p = param["p"];
  int k = param["k"];
  int N = param["N"];
  N += 1;
  arma::cube x_cross0(p,p,N);
  arma::cube x_cross1(p,p,N);
  arma::mat x(p,N);
  arma::mat A = Cinv.i()*B;
  arma::mat C = Cinv.i();
  int lag = 1;
  arma::mat P0 = param["P0"];
  List active = param["active"];
  List not_active = param["not_active"];
  List CL = param["CL"];
  arma::mat fm(p,N);
  arma::mat fmout(N,p);
  arma::mat smout(N,p);
  arma::cube fc(p,p,N);
  arma::cube predcov(p,p,N);
  arma::mat mup(p,N);
  arma::mat predex(p,N);
  arma::cube covp = zeros<arma::cube>(p,p,N);
  arma::cube cov_smooth(p,p,N);
  arma::cube cov2smooth(2*p,2*p,N);
  covp.fill(0);
  fm.col(lag-1).fill(0);
  fc.slice(lag-1) = P0;
  arma::mat eyep(p,p);
  eyep.eye();
  
  ///initialize everything
  predex.col(0).fill(0);
  predcov.slice(0) = P0;
  arma::mat CLi = as<arma::mat>(CL[0]);
  arma::colvec ytilde = as<arma::colvec>(Xsqueezed[0]) - CLi*predex.col(0);
  arma::mat S = CLi*predcov.slice(0)*CLi.t();

  arma::mat K = predcov.slice(0)*CLi.t()*inv(S);
  fm.col(0) = predex.col(0) + K*ytilde;
  fc.slice(0) = (eyep - K*CLi)*predcov.slice(0);
  
  
  for (int i = lag; i < N; i++) {
    arma::mat CLi = as<arma::mat>(CL[i]);
    for (int j = 0; j < p; j++) {
      covp(j,j,i) = 0;
      double tempmu = 0;
      double tempcov = 0;
      for (int q = 0; q < k; q++) {
        tempcov += as<double>(wrap(theta(i,j,q)/sigma(j,q)));
        tempmu += as<double>(wrap(theta(i,j,q)*mu(j,q)/sigma(j,q)));
      }
      mup(j,i) = tempmu*(1.0/tempcov);
      covp(j,j,i) = 1.0/(beta*tempcov);
    }
    mup.col(i) = C*mup.col(i);
    covp.slice(i) = C*covp.slice(i)*C.t();
    
    predex.col(i) = A * fm.col(i-1) + mup.col(i);
    predcov.slice(i) = A*fc.slice(i-1)*A.t() + covp.slice(i);
    
    arma::colvec ytilde = as<arma::colvec>(Xsqueezed[i]) - CLi*predex.col(i);
    arma::mat S = CLi*predcov.slice(i)*CLi.t();

    arma::mat K = predcov.slice(i)*CLi.t()*inv(S);
    fm.col(i) = predex.col(i) + K*ytilde;
    fc.slice(i) = (eyep - K*CLi)*predcov.slice(i);
  }
  //x = fm;
  //begin smoothing steps
  x.col(N-1) = fm.col(N-1);
  cov_smooth.slice(N-1) = fc.slice(N-1);
  x_cross0.slice(N-1) = cov_smooth.slice(N-1) + x.col(N-1)*x.col(N-1).t();
  for (int i = (N-2); i >= 0;i--){
    arma::mat Ji = fc.slice(i)*A.t()*predcov.slice(i+1).i();
    x.col(i) = fm.col(i) +  Ji*(x.col(i+1) - predex.col(i+1));
    cov_smooth.slice(i) = fc.slice(i) + Ji*(cov_smooth.slice(i+1) - predcov.slice(i+1))*Ji.t();
    x_cross0.slice(i) = cov_smooth.slice(i) + x.col(i)*x.col(i).t();
    x_cross1.slice(i+1) = (cov_smooth.slice(i+1)*Ji.t() + x.col(i+1)*x.col(i).t()).t();
    arma::mat temp(2*p,2*p);
    temp.submat(0,0,p-1,p-1) = cov_smooth.slice(i + 1);
    temp.submat(p,p,2*p-1,2*p-1) = cov_smooth.slice(i);
    temp.submat(0,p,p-1,2*p-1) = (Ji*cov_smooth.slice(i+1)).t();
    temp.submat(p,0,2*p-1,p-1) = Ji*cov_smooth.slice(i+1);

    cov2smooth.slice(i) = temp;
    
  }

  
  for (int j = 0; j < p; j++) {
      if (obs(N-1,j) == 1) {
        cov2smooth.slice(N-2)(j,j) = 1;
      }
  }  
    
  
  for (int i = (N-2); i >= 0;i--){
    for (int j = 0; j < p; j++) {
      if (obs(i,j) == 1) {
        //showValue_int(i);
        //showValue_int(j);

        cov2smooth.slice(i)(p + j,p + j) = 1;
        cov_smooth.slice(i)(j,j) = 1;
        if (i > 0) {
          cov2smooth.slice(i-1)(j,j) = 1;

        }

      } 
    }
  }
  
  NumericVector x_cross0n = as<NumericVector>(wrap(x_cross0));
  NumericVector x_cross1n = as<NumericVector>(wrap(x_cross1));
  NumericVector cov_smoothn = as<NumericVector>(wrap(cov_smooth));
  NumericVector cov2smoothn = as<NumericVector>(wrap(cov2smooth));
  return List::create(x_cross0n,x_cross1n,x,fm,cov_smoothn,cov2smoothn);
}

double compute_ELBO2(arma::mat x, arma::cube x_cross0, arma::cube x_cross1, arma::mat mu, arma::mat sigma, arma::mat B, arma::cube theta, arma::mat Cinv,int N, int p, int k,arma::cube cov2smooth,arma::cube covsmooth,arma::mat pi) {
  double elbo = 0.0;
  double val = det(Cinv);
  elbo += (N-1)*log(abs(val));
  for (int i = 1; i <= N;i++) {
    for (int j = 0;j < p;j++) {
      for (int l = 0;l < k;l++) {
        double temp = -as<double>(wrap((Cinv.row(j)*x.col(i) - B.row(j)*x.col(i-1) - mu(j,l))*(Cinv.row(j)*x.col(i) - B.row(j)*x.col(i-1) - mu(j,l))));
        elbo += temp*(theta(i,j,l)/(2.0*sigma(j,l)));
        elbo += -.5*as<double>(wrap(theta(i,j,l)))*log(sigma(j,l));
      }
    }
  }
  return(elbo);
}

double compute_ELBO_part(arma::mat x, arma::cube x_cross0, arma::cube x_cross1, arma::mat mu, arma::mat sigma, arma::mat B, arma::cube theta, arma::mat Cinv,int N, int p, int k){
  double elbo = 0.0;
  //double val;
  //double sign;
  double val = det(Cinv);
  elbo += (N-1)*log(fabs(val));
  for (int i = 1; i <= N;i++) {
    for (int j = 0;j < p;j++) {
      for (int l = 0;l < k;l++) {
        double temp = 0.0;
        double temp2 = 0.0;
        temp += -as<double>(wrap(Cinv.row(j)*x_cross0.slice(i)*Cinv.row(j).t()));
        temp += as<double>(wrap(2*Cinv.row(j)*x_cross1.slice(i).t()*B.row(j).t()));
        temp += as<double>(wrap(2*Cinv.row(j)*x.col(i)*mu(j,l)));
        temp += -as<double>(wrap(2*B.row(j)*x.col(i-1)*mu(j,l)));
        temp += -as<double>(wrap(B.row(j)*x_cross0.slice(i-1)*B.row(j).t()));
        temp += -as<double>(wrap(mu(j,l)*mu(j,l)));
        temp2 = temp*(as<double>(wrap(theta(i,j,l)))/as<double>(wrap(2.0*sigma(j,l))));
        elbo += temp2;
      }
    }
  }
  return(elbo);
}

double compute_ELBO(arma::mat x, arma::cube x_cross0, arma::cube x_cross1, arma::mat mu, arma::mat sigma, arma::mat B, arma::cube theta, arma::mat Cinv,int N, int p, int k,arma::cube cov2smooth,arma::cube covsmooth,arma::mat pi,double beta) {
  double elbo = 0.0;
  double val = det(Cinv);
  double val2 = fabs(val);
  elbo += (N-1)*log(val2);

  for (int i = 1; i <= N;i++) {
    for (int j = 0;j < p;j++) {
      for (int l = 0;l < k;l++) {
        double temp = 0.0;
        double temp2 = 0.0;
        temp += -as<double>(wrap(Cinv.row(j)*x_cross0.slice(i)*Cinv.row(j).t()));
        temp += as<double>(wrap(2*Cinv.row(j)*x_cross1.slice(i).t()*B.row(j).t()));

        temp += as<double>(wrap(2*Cinv.row(j)*x.col(i)*mu(j,l)));
        temp += -as<double>(wrap(2*B.row(j)*x.col(i-1)*mu(j,l)));

        temp += -as<double>(wrap(B.row(j)*x_cross0.slice(i-1)*B.row(j).t()));
        temp += -as<double>(wrap(mu(j,l)*mu(j,l)));

        temp2 = temp*(as<double>(wrap(theta(i,j,l)))/as<double>(wrap(2.0*sigma(j,l))));
        temp2 += -.5*as<double>(wrap(theta(i,j,l)))*log(sigma(j,l));
        temp2 += theta(i,j,l)*log(pi(j,l));
        //showValue(temp2);

        if (theta(i,j,l) > 0) {
          temp2 += -(1.0/beta)*theta(i,j,l)*log(theta(i,j,l));
        }
        //showValue(pi(j,l));
        elbo += temp2;
       }

    }

  }
//  //add the entropy term for the state space
//  elbo += (1.0/beta)*.5*log(det(covsmooth.slice(0)));
//  //showValue(elbo);
//  for (int i = 0; i < (N-1); i++) {
//    elbo += (1.0/beta)*.5*log(det(cov2smooth.slice(i))) - (1.0/beta)*.5*log(det(covsmooth.slice(i)));
//
//    //showValue(elbo);
//  }
  
  return(elbo);
}

arma::mat optimize_Cinv(arma::mat x, arma::cube x_cross0, arma::cube x_cross1, arma::mat mu, arma::mat sigma, int p, int k, int N,arma::mat B, arma::cube theta,arma::mat Cinv,double lamC) {
  mat mask = ones<mat>(p,p);
  for (int j = 0; j < p; j++) {
    for (int i = (j + 1); i < p; i++) {
      mask(j,i) = 0;
    }
  }
  mat A = Cinv.i() * B;
  double lambda = lamC;
  double diff = 100;
  double C_thresh = .0001;
  double stepsize = .0000002;
  int iter = 0;
  mat perm_mat = zeros<mat>(p*p,p*p);
  for (int i = 1; i <= p*p;i++) {
    int temp = (i - 1) % p;
    int temp2 = (i - 1)/p + 1;
    perm_mat(i-1,(temp)*p + temp2 - 1) = 1;
  }
  arma::mat hesslam = zeros<mat>(p*p,p*p);
  for (int i = 0; i< p*p;i++) {
    hesslam(i,i) = -lambda;
  }
  while (diff > C_thresh) {
    iter += 1;
    //showValue(iter);
    mat grad = zeros<mat>(p,p);
    vec gradv = zeros<vec>(p*p);
    mat hess = zeros<mat>(p*p,p*p);
    grad += (N - 1)*Cinv.i().t();
    grad += -lambda*Cinv;
    gradv += vectorise(grad);
    hess += -(N-1)*perm_mat*kron(Cinv.i().t(),Cinv.i());
    hess += hesslam;
    vec Cinv_vec = vectorise(Cinv);
    for (int n = 1; n <= N; n++) {
      for (int i = 0; i < k;i++) {
        mat weights = zeros<mat>(p,p);
        for (int j = 0;j < p;j++) {
          weights(j,j) = theta(n,j,i)/(sigma(j,i));
        }
        vec temp = x.col(n) - A*x.col(n-1);
        mat tempmult = x_cross0.slice(n) - x_cross1.slice(n).t()*A.t() + A*x_cross0.slice(n-1)*A.t() - A*x_cross1.slice(n);
        grad += -weights*(Cinv*(tempmult));
        grad += weights*mu.col(i)*temp.t();
        hess += -kron(tempmult,weights);
      }
    }
    
    
    gradv = vectorise(grad);
    mat BB = Cinv*A;
    uvec inds = find(gradv);
    arma::vec gradv2 = gradv(inds);
    arma::mat hess2 = hess.submat(inds,inds); 
//    ////////////////line search////////////
//    double normed = norm(grad % mask,"fro");
//    double mm = -normed*normed;
//    double alpha = .1;
//    double tau = .5;
//    double c = .5;
//    double t = -c*mm;
//    double f_curr = compute_ELBO_part(x,x_cross0,x_cross1,mu,sigma,BB,theta,Cinv,N,p,k);
//    bool cond = true;
//    while (cond) {
//      alpha *= tau;
//      mat Cinvnew = Cinv + alpha*grad;
//      mat BBnew = Cinvnew*A;
//      double f_n = compute_ELBO_part(x,x_cross0,x_cross1,mu,sigma,BBnew,theta,Cinvnew,N,p,k);
//      if (-(f_curr - f_n) >= alpha*t) {
//        cond = false;
//      }
//      //showValue(alpha);
//    }
//    //showValue(alpha);
//    
//    //showMatrix(grad);
//    Cinv = Cinv + alpha*(grad % mask);
    
    //Cinv = Cinv + stepsize*(grad % mask);
    //showValue(11);
    
    vec direct = hess.i()*gradv;
    Cinv_vec -= direct;
    mat temp;
    temp.insert_cols(0,Cinv_vec);
    temp.reshape(p,p);
    Cinv = temp;
    diff = norm(grad,"fro");
    diff = norm(direct);
    if (iter > 5000) {
      diff = 0;
    }
  }
  //showValue(iter);
  return Cinv;
}

//[[Rcpp::export]]
List opt_var_var_cpp(List param,List Xsqueezed) {
  int p = (int)param["p"];
  int k = (int)param["k"];
  int N = (int)param["N"];
  double lamsig = (double)param["lamSig"];
  double lamC = (double)param["lamC"];
  arma::mat pi = as<arma::mat>(param["pi"]);
  arma::mat A = as<arma::mat>(param["A"]);
  arma::mat C = as<arma::mat>(param["C"]);
  arma::mat xt = as<arma::mat>(param["x"]);
  arma::mat mu = as<arma::mat>(param["mu"]);
  arma::mat sigma = as<arma::mat>(param["sigma"]);
  NumericVector theta_r = as<NumericVector>(param["theta"]);
  IntegerVector theta_dim= theta_r.attr("dim");
  arma::mat obs = as<arma::mat>(param["observed"]);
  arma::cube theta(theta_r.begin(), theta_dim[0], theta_dim[1], theta_dim[2], false);

  //showValue_int(N);
  arma::mat x = xt.t();
  arma::mat Cinv = C.i();
  for (int i = 0; i < p; i++) {
    Cinv(i,i) = 1;
  }
  arma::mat B = Cinv * A;
  showMatrix(B);

  
  bool missing = true;
  //missing = false;
  //showValue(1);
  double diff = 100;
  double eps = .0001;
  double beta = 1;
  double beta_factor = 1.3;
  arma::mat pi_old = pi;
  arma::mat B_old = B;
  arma::mat Cinv_old = Cinv;
  arma::mat mu_old = mu;
  arma::mat sigma_old = sigma; 
  //showMatrix(Cinv);
  //showMatrix(Cinv);
  arma::mat fm(N,p);
  List x_cross = calc_moments(x,N,p,k);

  NumericVector x_cross0n = as<NumericVector>(x_cross[0]);
  NumericVector x_cross1n = as<NumericVector>(x_cross[1]);
  IntegerVector arrayDims0n = x_cross0n.attr("dim");
  arma::cube x_cross0(x_cross0n.begin(), arrayDims0n[0], arrayDims0n[1], arrayDims0n[2], false);
  IntegerVector arrayDims1n = x_cross1n.attr("dim");
  arma::cube x_cross1(x_cross1n.begin(), arrayDims1n[0], arrayDims1n[1], arrayDims1n[2], false);
  //arma::cube theta = zeros<cube>(N,p,k); 
  arma::cube cov2smooth(2*p,2*p,N);
  arma::cube covsmooth(p,p,N);
  
  //arma::cube x_cross0t = x_cross0;
  //arma::cube x_cross1t = x_cross1;
  arma::mat xtt = x;
  
  int iter = 1;
  std::list<double> mylist;
  //double elbon = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi);
  //mylist.push_back(elbon); 
  //showValue(2.0);
  //while (iter < 30) {
  double elbon = 0.0;
  double elbon_old = 0.0;
  double elbdiff = 0.0;
    diff = 100;

  elbon = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
  mylist.push_back(elbon);
  while (diff < eps) {
    //showValue_int(iter);
    iter += 1;
    pi_old = pi;
    B_old = B;
    Cinv_old = Cinv;
    mu_old = mu;
    sigma_old = sigma;
    //showMatrix(x_cross0.slice(1));
    
    double elbon_ot = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);

    theta = optimize_theta(x_cross0,x_cross1,x,mu,sigma,B,Cinv,pi,p,k,N,beta);
    double elbon_pt = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
    double diff_t = elbon_pt - elbon_ot;
    //showValue(diff_t);
    //showValue(elbon_ot);
     //   B = optimize_B(x,x_cross0, x_cross1,mu,sigma,p,k,N,Cinv,theta);

    //showValue(elbon_pt);
    //if (diff_t < 0) {
    //  showValue(elbon_pt-elbon_ot);
    //  showValue(1);
    //}
    
    //B = optimize_B(x,x_cross0, x_cross1,mu,sigma,p,k,N,Cinv,theta);

    
    //B = optimize_B(x,x_cross0, x_cross1,mu,sigma,p,k,N,Cinv,theta);


    //////////////////////////
    //if (missing) {
      x_cross = forward_backward_cpp(theta,mu,sigma,B,Cinv,Xsqueezed,param,obs,beta);
      NumericVector x_cross0n = as<NumericVector>(x_cross[0]);
      NumericVector x_cross1n = as<NumericVector>(x_cross[1]);
      NumericVector covsmoothn = as<NumericVector>(x_cross[4]);
      NumericVector cov2smoothn = as<NumericVector>(x_cross[5]);
      IntegerVector arrayDims0n = x_cross0n.attr("dim");
      arma::cube x_cross0t(x_cross0n.begin(), arrayDims0n[0], arrayDims0n[1], arrayDims0n[2], false);
      IntegerVector arrayDims1n = x_cross1n.attr("dim");
      arma::cube x_cross1t(x_cross1n.begin(), arrayDims1n[0], arrayDims1n[1], arrayDims1n[2], false);
      IntegerVector arrayDimscovs = covsmoothn.attr("dim");
      arma::cube covsmootht(covsmoothn.begin(), arrayDimscovs[0], arrayDimscovs[1], arrayDimscovs[2], false);
      IntegerVector arrayDimscovs2 = cov2smoothn.attr("dim");
      arma::cube cov2smootht(cov2smoothn.begin(), arrayDimscovs2[0], arrayDimscovs2[1], arrayDimscovs2[2], false);
      x = as<arma::mat>(x_cross[2]);
      fm = as<arma::mat>(x_cross[3]);
      //showMatrix(cov2smooth.slice(3));
    //}
    covsmooth = covsmootht;
    cov2smooth = cov2smootht;
          //showMatrix(cov2smooth.slice(3));
      x_cross0 = x_cross0t;
      x_cross1 = x_cross1t;
      //x = xtt;     
          
      //double elbon_x = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
      //double diff_x = elbon_x - elbon_pt;
      //showValue(diff_x);
      //showValue(0);
      //if (diff_x < 0) {
      //  showValue(diff_x);
      //  showValue(2);
      //}
    ////////////////////////////////////
      
    //double elbon_mup = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
    //mu = optimize_mu(x,theta,Cinv,B,k,p,N);
    //double elbon_mua = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
    //double diff_mu = elbon_mua - elbon_mup;
    //showValue(diff_mu);
    //if (diff_mu < 0) {
    //  showValue(diff_mu);
    //  showValue(3);
    //}
    
    B = optimize_B(x,x_cross0, x_cross1,mu,sigma,p,k,N,Cinv,theta);
    //double elbon_B = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
    //double diff_B = elbon_B - elbon_x;
      //showValue(diff_B);
      //if (diff_B < 0) {
      //  showValue(diff_B);
      //  showValue(3);
      //}
    
    //pi = optimize_pi(theta,k, p,N);
    //showMatrix(pi);
//    double elbon_mup = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
//    mu = optimize_mu(x,theta,Cinv,B,k,p,N);
//    double elbon_mua = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
//    double diff_mu = elbon_mua - elbon_mup;
//    //showValue(diff_mu);
//    if (diff_mu < 0) {
//      showValue(diff_mu);
//      showValue(3);
//    }
          
    //sigma = optimize_sigma(x,x_cross0,x_cross1,theta,B,Cinv,mu,N,p,k);

    mat Cinv_old = Cinv;
    //Cinv = optimize_Cinv(x,x_cross0,x_cross1,mu,sigma,p,k,N,B,theta,Cinv,lamC);
    elbon_old = elbon + 0.0;
    //A = Cinv_old.i()*B;
    //B = Cinv*A;
    //elbon = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi);
    
    //B = optimize_B(x,x_cross0, x_cross1,mu,sigma,p,k,N,Cinv,theta);

    diff = calc_differences(pi_old,pi,B_old,B,Cinv_old,Cinv,mu_old,mu,sigma_old,sigma);
    elbon_old = elbon;
    elbon = compute_ELBO(x,x_cross0,x_cross1,mu,sigma,B,theta,Cinv,N,p,k,cov2smooth,covsmooth,pi,beta);
    mylist.push_back(elbon); 
    double elb_diff = elbon - elbon_old;
    if (iter > 5000) {
      diff = 0;
    }
  }
  
  showMatrix(Cinv);
  C = Cinv.i();
  A = C * B;
  return Rcpp::List::create(Rcpp::Named("mu") = mu,
                            Rcpp::Named("sigma") = sigma,
                            Rcpp::Named("pi") = pi,
                            Rcpp::Named("A") = A,
                            Rcpp::Named("C") = C,
                            Rcpp::Named("x") = x,
                            Rcpp::Named("xf") = fm,
                            Rcpp::Named("elbo") = mylist,
                            Rcpp::Named("theta") = theta);
}
