from itertools import product
import os

Quart_inds = [1]
month_inds = [1,2,3,4,5,6,7,8]
standardize = [1]
shorf_trans = [1]
nclust = [1]
#["pair0049.txt","pair0050.txt","pair0051.txt"]
#,"pair0065.txt","pair0066.txt","pair0067.txt"]
#realdata = ["pair0068.txt","pair0096.txt","pair0072.txt","pair0087.txt","pair0099.txt"]

basejfile = "EM_slurm.sbatch"
par_prod = product(Quart_inds,month_inds,standardize,shorf_trans,nclust)
for par in par_prod:
    qi,mi,st,shorf,ncl  = par
    jfile_b = "qi-%d" % (qi) + "-mi-%d" % (mi) + "-st-%d" % (st) + "-shorf-%d"% (shorf) + "-nclust-%d" % (ncl)
    jfile = "sbatch_files/" + jfile_b + basejfile
    with open(jfile,'w') as f:
        f.write("#!/bin/bash" + "\n")
        f.write("\n")
        f.write("#BATCH --job-name tankjob" + "\n")
        f.write("#SBATCH --partition short" + "\n")
        f.write("#SBATCH --ntasks 1" + "\n")
        f.write("#SBATCH --time 0-08:00" + "\n")
        f.write("#SBATCH --mem-per-cpu=4000M" + "\n")
        f.write("#SBATCH -o outs/" + jfile_b + "EM_sim_%j.out" +"\n")
        f.write("#SBATCH -e outs/" + jfile_b + "EM_sim_%j.err" + "\n")
        f.write("#SBATCH --mail-user=alextank@uw.edu" + "\n")
        f.write("\n")
        args = "'--args " +  "month_ind=%d " %(mi) + "Quart_ind=%d " %(qi) + "standardize=%d "%(st) + "nclust=%d " %(ncl) +"shorf_trans=%d '" %(shorf)
        temp =  "%d" % (qi) + "-%d" % (mi) + "-%d" % (st) + "-%d" % (shorf) + "-%d" % (ncl)
        f.write("R CMD BATCH " + args + " realdat_mf_EM.R " + "outs/" + temp + '_' + '"${SLURM_ARRAY_TASK_ID}".Rout')
    os.chmod(jfile,0755)

jfile_r = "master_EM_subsamp_slurm_realdat_mf.sh"
with open(jfile_r,'w') as f:
    f.write("#!/bin/bash" + "\n")
    f.write("\n")
    par_prod = product(Quart_inds,month_inds,standardize,shorf_trans,nclust)
    for par in par_prod:
        qi,mi,st,shorf,ncl = par
        jfile_b = "sbatch_files/" + "qi-%d" % (qi) + "-mi-%d" % (mi) + "-st-%d"% (st) + "-shorf-%d" % (shorf) + "-nclust-%d" % (ncl)
        jfile = jfile_b + basejfile
        f.write("sbatch --array=1-1000 " + jfile + "\n")
os.chmod(jfile_r,0755)


