#setwd("/home/alextank/Documents/testgraph/graphirregular/code/Rcodevar")
setwd("/Users/alextank/graphirregular/code/Rcodevar")
library("Rcpp")
library("RcppArmadillo")
source("mixedfreq_var.R")
source("interpolate.R")
source("visualize_results.R")
source("VAR_subsamp.R")
sourceCpp("struct_var_var.cpp")
source("simulation_helper_funcs.R")
source("subsampVAR_R.R")

# command = 0
# if (command == 1) {
#   ######load parameters######
#   baseind = 5
#   print("compiled")
#   args=commandArgs()
#   seed = 933*as.integer(args[baseind+1])
#   k = as.integer(args[baseind+2])
#   N = as.integer(args[baseind+3])
#   sd_errors = as.double(args[baseind+4])
#   spikenslab = as.integer(args[baseind+5])
#   sampling_type = as.integer(args[baseind+6])
#   A_type = as.integer(args[baseind+7])
#   p = as.integer(args[baseind+8])
#   alpha = as.double(args[baseind+9])
#   mu_sig = as.double(args[baseind+10])
#   shape = as.double(args[baseind+11])
#   rate = as.double(args[baseind+12])
#   C_type = as.integer(args[baseind+13])
#   
#   print(c(seed,k,N,sd_errors,spikenslab,sampling_type,A_type,p,alpha,mu_sig,shape,rate,C_type))
# }
# if (command == 0) {
  seed = 88979833
  seed = 438458334
  k = 2
  m_type = 2
  N = 699
  sd_errors = 4
  sampling_type = 3
  A_type = 1
  C_type = 1
  p = 2
lamC = 0
lamSig = 10
#}

#prefix = "subsampVar"
#outfile = paste(prefix,"_",seed,"_",k,"_",N,"_",sd_errors,"_",spikenslab,"_",sampling_type,"_",A_type,"_",p,"_",C_type,".csv",sep="")

#########begin simulation#########
set.seed(seed)
sampling = sampling_select(sampling_type,p)
A = A_select(A_type,p)
C = C_select(C_type,p)
p = length(sampling)
simulation = list()

simulation$err_type = 'Gaussian_Mixture'
simulation$k = k;
simulation$sigma = .1;
simulation$lag = 1;
simulation$sd = .25;
simulation$p = p;
simulation$C = C;
simulation$m_type = m_type;
#simulation$alpha = alpha;
#simulation$mu_sig = mu_sig;
#simulation$shape = shape;
#simulation$rate = rate;

#output = VAR_subsamp_scale(N,sampling,A,simulation,sd_errors);
output = VAR_subsamp_scale_fixed_errs(N,sampling,A,simulation,sd_errors,C);

X = output$x
z = output$z
mean_err = output$mean_err
var_err = output$var_err
xtrue = output$xtrue
simulation$mu = output$mum
simulation$sig = output$sig
simulation$theta = output$theta

####start sampler#####
param = list()
param$A = A
param$z = z
#param$A = matrix(rnorm(p*p,0,1)^2,p,p)
param$p = p
param$xinit
xinit = interpolate_block(X,p,N)
xinit2 = init_mid(X,N,p)
param$xinit = xinit;
param$k = simulation$k
param$thetam = output$thetam
simulation$pi = output$thetam
param$mu = output$mum
#param$mu = matrix(rnorm(param$p*param$k,0,1),param$p,param$k)
param$sigma = output$sigmam*output$sigmam
simulation$sigma = param$sigma
param$lag = 0
param$N = N - 1
param$C = C
#param$mum = matrix(rnorm(param$p*param$k,0,1),param$p,param$k)
#param$sigmam = matrix(rnorm(param$p*param$k,0,1)^2,param$p,param$k)
param$pi = param$thetam
#param$pi = rdirichlet(param$p,rep(1,param$k)*10)
#param$thetam = rdirichlet(param$p,rep(1,param$k)*10)
param$P0 = diag(rep(1000,p))

#for (i in 1:p) {
#  base = param$sigma[i,1]
#  param$sigma[i,] = param$sigma[i,]/base
#  param$mu[i,] = param$mu[i,]/sqrt(base)
#  param$C[,i] = param$C[,i]*sqrt(base)
#}

simulation=param

if (param$k == 1) {
  thetam = rep(1,p);
  mum = rep(0,p);
} 
reps = 50
currbest = -1000000000
outputt = 0
elblist = rep(0,reps)
param$lamC = lamC
param$lamSig = lamSig
nc = rep(0,reps)
for (i in 1:reps) {
  set.seed(2311 + i*233);
  #param$xinit = xtrue
  param$xinit = xinit
  print(i);
  if (i > 1) {
  param$xinit = xinit
  param$A = matrix(rnorm(param$p^2,0,1),param$p,param$p)
  param$A[1,1] = abs(param$A[1,1])
  param$A[2,2] = abs(param$A[2,2])
  #param$A = diag(c(.99,.99))
  #param$mu = stm
  #ma = max(abs(eigen(param$A)$values))
  #param$A = param$A/(ma*1.3)
  
  
  #param$mu = matrix(rnorm(param$p*param$k,0,.2),param$p,param$k)
  #param$mu = param$mu - apply(param$mu,1,mean)
  #param$pi = rdirichlet(param$p,rep(1,param$k)*1)
  #for (j in 1:p) {
  #  mmu = sum(param$mu[j,]*param$pi[j,])
  #  param$mu[j,] = param$mu[j,] - mmu
  #}
  #param$sigma = matrix(rnorm(param$p*param$k,0,1)^2,param$p,param$k) + .001
  #param$sigma[,1] = 1
  
  #param$C = matrix(rnorm(param$p^2,0,1),param$p,param$p)
  #param$C[1,2] = 0
  #param$C[2,1] = 0
  #param$C[1,1] = abs(param$C[1,1])
  #param$C[2,2] = abs(param$C[2,2])
  }
  #param$C[1,1] = 1
  #param$C[2,2] = 1
  #param$C[1,1] = .9815032
  #param$C[1,2] = -.0347548ç
  #param$C[2,] = c(-.0006823151,1.0252848)
  #param$A = matrix(rnorm(param$p^2,0,1),param$p,param$p)
  #output = try(mixedfreq_var(X,param))
  param$theta = convert_z_to_theta(param$z,param$N,param$p,param$k)
  output = try(subsampvar_t(param$mu,param$sigma,param$theta,X,param$pi,t(param$xinit),param$C,param$A,N,p,k))
  #print(output$elbo)
  #print(output[[8]][length(output[[8]])])
  #print(output$pi)
  #print(output$mu)
  #print(output$sigma)
  if (i == 1) {
    output_init = output
  } 
  else{ 
    if (class(output) != "try-error") {
    nc[i] = length(output[[7]])
    elblist[i] = output[[7]][length(output[[7]])]
    if (!is.nan(output[[7]][length(output[[7]])])) {
    if (output[[7]][length(output[[7]])] > currbest) {
      currparam = param
      currbest = output[[7]][length(output[[7]])]
      outputt = output
    }
  }
  }
  }
  #print(output$mu)
}
Covsim = Compute_Covariance(simulation$C,simulation$sigma,simulation$mu,simulation$pi,param$p,param$k)
Covtt = Compute_Covariance(outputt$C,outputt$sig,outputt$mu,outputt$pi,param$p,param$k)
Covt = Compute_Covariance(output$C,output$sig,output$mu,output$pi,param$p,param$k)

outputt$A %*% Covtt %*% t(outputt$A) + Covtt
outputt$A %*% outputt$A

A %*% Covsim %*% t(A) + Covsim
A %*% A

output$A %*% Covt %*% t(output$A) + Covt
output$A %*% output$A

#print(output[[8]])
#plot(output[[8]][12:20],type='l')
#print(output[[1]])
#print(output[[3]])

